
### ⚠️ Please don't host this code on GitHub! <a href="https://sfconservancy.org/GiveUpGitHub/">More information</a>

<h1 align="center">💻 CPlusPatch Web </h1>

<h4 align="center">A work-in-progress personal website, portfolio and CMS built with Next.js and TypeScript</h4>

<p align="center">
 	<img src="https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white">
	<img src="https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white">
	<img src="https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white">
	<img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white">
	<img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB">
	<img src="https://img.shields.io/badge/Markdown-000000?style=for-the-badge&logo=markdown&logoColor=white">
	<img src="https://img.shields.io/badge/Yarn-2C8EBB?style=for-the-badge&logo=yarn&logoColor=white">
	<img src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white">
	<br/>
	<br/>
	<img src="https://img.shields.io/badge/eslint-3A33D1?style=for-the-badge&logo=eslint&logoColor=white">
	<img src="https://img.shields.io/badge/prettier-1A2C34?style=for-the-badge&logo=prettier&logoColor=F7BA3E">
	<img src="https://img.shields.io/badge/Vercel-000000?style=for-the-badge&logo=vercel&logoColor=white">
	<img src="https://img.shields.io/badge/Supabase-181818?style=for-the-badge&logo=supabase&logoColor=white">
	<img src="https://img.shields.io/badge/VSCode_OSS-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white">
	<br/>
	<br/>
	<img src="https://img.shields.io/badge/Made_on-Linux-1793D1?style=for-the-badge&logo=arch-linux&logoColor=white">
	<img src="https://img.shields.io/badge/Built_for-Firefox_Browser-FF7139?style=for-the-badge&logo=Firefox-Browser&logoColor=white">
</p>

![screenshot](https://i.imgur.com/3QC4Vbc.png)
![screenshot](https://i.imgur.com/GBMQrQ4.png)
![screenshot](https://i.imgur.com/8BnSwKB.png)
![screenshot](https://i.imgur.com/jmTejGB.png)
![screenshot](https://i.imgur.com/jKw1zYP.png)

## Key Features

* Lightning-fast, responsive and secure
* Server-side rendering allowing for smaller JS payloads and better website metrics
* Optimized as much as possible
* Full user authentication with password resets, profile editing, avatar upload, user roles (such as admin)
* Admin dashboard and user manager
* Full CRUD with Markdown (on a hidden page)
* < 200 kB initial JS payload for every page (I am looking onto reducing this)

## Planned features

* ✅ ~~Full user auth~~ Done!
* ✅ ~~User roles~~ Done!
* ✅ ~~Full Markdown CRUD~~ Done!
* ✅ ~~User profiles~~ Done!
* Page theming - Being worked on right now

## How to access the CMS?

Simply click the "CMS" link on site navbar or visit `/blog` in a web browser

## Getting your own version setup

## Downloading and running

Install Node.js (version 18 LTS is recommended for this project) and Git, then clone the repository:

```bash
# Clone this repository
$ git clone https://codeberg.org/CPlusPatch/cpluspatch-cms-next

# Go into the repository
$ cd cpluspatch-cms-next

# Install dependencies
$ yarn

# Run the app
$ yarn dev
```

### Database setup

1. Create a new project at https://supabase.com
2. Open the project's Table Editor and create two new tables named `accounts` and `user`. Set up these columns:
```ts
export interface Accounts {
  	id: number
	name: string | null
	username: string | null
	avatar: string | null
	// This should be linked to the "id" column of the "users"
	// table (change schema to "auth" to see it)
	user_id: string | null // ⬅ linked to auth:users:id
	role: string | null
	id: number
    about_me: string | null
}

export interface Posts {
	id: number
	created_at: string | null
	content: string | null
	author: number | null
	title: string | null
	description: string | null
	slug: string | null
	visibility: string | null
	banner: string | null
}
```

### Environment variables

Copy `.env.example` to `.env`:

```sh
cp .env.example .env
```
Setup Supabase with the previously mentioned database schema and then fill in your API keys (anon and master) inside the .env file.
Then, create an account at https://imagekit.io and fill in your endpoint and API keys (both public and private).
Don't forget to change `NEXT_PUBLIC_HOSTNAME` to your app's public URL! (`http://localhost:3000` for a dev server)

## Development

### Generating types

Install the Supabase CLI and run the following command to generate up-to-date types:
```sh
supabase gen types typescript --project-id "$PROJECT_ID" --schema public > types/supabase.ts
```
NOTE: Please check `types/types.d.ts`

## Credits

This software uses the following open source libraries, and more! Check out `package.json` for a list of all the librarues

- [Next.js](https://nextjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Supabase](https://supabase.com/)

## License

Please see `COPYING` for more details.
This software is licensed under GNU GPLv3

---
