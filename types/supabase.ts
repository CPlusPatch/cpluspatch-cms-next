export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      accounts: {
        Row: {
          id: number
          name: string | null
          username: string | null
          avatar: string | null
          user_id: string | null
          role: string | null
          about_me: string | null
        }
        Insert: {
          id?: number
          name?: string | null
          username?: string | null
          avatar?: string | null
          user_id?: string | null
          role?: string | null
          about_me?: string | null
        }
        Update: {
          id?: number
          name?: string | null
          username?: string | null
          avatar?: string | null
          user_id?: string | null
          role?: string | null
          about_me?: string | null
        }
      }
      posts: {
        Row: {
          id: number
          created_at: string | null
          content: string | null
          author: number | null
          title: string | null
          description: string | null
          slug: string | null
          visibility: string | null
          banner: string | null
        }
        Insert: {
          id?: number
          created_at?: string | null
          content?: string | null
          author?: number | null
          title?: string | null
          description?: string | null
          slug?: string | null
          visibility?: string | null
          banner?: string | null
        }
        Update: {
          id?: number
          created_at?: string | null
          content?: string | null
          author?: number | null
          title?: string | null
          description?: string | null
          slug?: string | null
          visibility?: string | null
          banner?: string | null
        }
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
  }
}
