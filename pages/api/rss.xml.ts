import type { NextApiRequest, NextApiResponse } from "next";
import { fetchPostsAsPublic } from "utils/database";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	const posts = await fetchPostsAsPublic();

	res.setHeader("Content-Type", "application/rss+xml");

	return res.status(200).send(`<?xml version="1.0" encoding="UTF-8" ?>
	<rss version="2.0">

	<channel>
		<title>CPlusPatch's Blog</title>
		<link>${process.env.NEXT_PUBLIC_HOSTNAME}</link>
		<description>A terrible blog written 100% from scratch in JavaScript</description>
		${
			posts
				? posts.map(
						post => `
			<item>
				<title>${post.title}</title>
				<link>${process.env.NEXT_PUBLIC_HOSTNAME}/blog/${post.slug}</link>
				<guid>${process.env.NEXT_PUBLIC_HOSTNAME}/blog/${post.slug}</guid>
				<author>contact@cpluspatch.com</author>
				<description>New RSS tutorial on W3Schools</description>
			</item>
		`,
				  )
				: ""
		}
	</channel>

	</rss> 
	`);
}

export {};
