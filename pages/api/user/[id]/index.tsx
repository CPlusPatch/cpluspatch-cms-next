import type { NextApiRequest, NextApiResponse } from "next";
import { editAccount, fetchPost, getServerSession } from "utils/database";

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const session = await getServerSession(req, res);
	if (!session) {
		return res.status(403);
	}

	const id = req.query.id.toString();

	if (req.method === "PUT" && id === session.user.id) {
		if (await editAccount("user_id", id, req.body)) {
			return res.status(200).json({
				success: true,
			});
		} else {
			return res.status(500).json({
				success: false,
				error: "Server error",
			});
		}
	} else {
		return res.status(403);
	}
}

export {};
