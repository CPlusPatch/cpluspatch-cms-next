import type { NextApiRequest, NextApiResponse } from "next";
import {
	deletePost,
	deleteUser,
	fetchAccountByUser,
	getServerSession,
} from "utils/database";

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const session = await getServerSession(req, res);
	if (!session) {
		return res.status(403);
	}

	const account = await fetchAccountByUser(session.user);

	if (account.role !== "admin") {
		return res.status(403);
	}

	const id = req.query.id.toString();

	if (await deleteUser(id)) {
		res.status(200).json({
			success: true,
		});
	} else {
		res.status(500).json({
			success: false,
			error: "Server error/user not found",
		});
	}
}

export {};
