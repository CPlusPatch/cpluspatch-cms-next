import type { NextApiRequest, NextApiResponse } from "next";
import { createNewAccount, fetchAccountByUser, fetchAllUsers } from "utils/database";
import { getServerSession } from "utils/database";

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const session = await getServerSession(req, res);
	if (!session) {
		return res.status(403);
	}
	const account = await fetchAccountByUser(session.user);
	if (account?.role !== "admin") {
		return res.status(403);
	}

	const body = JSON.parse(req.body);

	const email = body.email;
	const password = body.password;
	const username = body.username;
	const name = body.name;

	const response = await createNewAccount(email, password, username, name);

	if (response) {
		return res.status(201).json(response);
	}
	return res.status(500).json;
}
