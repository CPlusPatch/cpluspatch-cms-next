import type { NextApiRequest, NextApiResponse } from "next";
import { fetchAccountByUser, fetchAllUsers } from "utils/database";
import { getServerSession } from "utils/database";

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const session = await getServerSession(req, res);
	if (!session) {
		return res.status(403);
	}
	const account = await fetchAccountByUser(session.user);
	if (account?.role !== "admin") {
		return res.status(403);
	}

	res.status(200).json(await fetchAllUsers());
}
