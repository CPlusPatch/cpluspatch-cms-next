import type { NextApiRequest, NextApiResponse } from "next";
import { editPost, fetchPost, getServerSession } from "utils/database";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	const session = await getServerSession(req, res);
	if (!session) {
		return res.status(403);
	}

	const id = req.query.id.toString();
	const post = await fetchPost("id", id);

	if (req.method === "GET") {
		if (post) {
			res.status(200).json({
				success: true,
				post: post,
			});
		} else {
			res.status(404).json({
				success: false,
				error: "Post not found",
			});
		}
	} else if (req.method === "PUT" && post && post.author.user_id === session.user.id) {
		if (await editPost("id", id, req.body)) {
			res.status(200).json({
				success: true,
			});
		} else {
			res.status(500).json({
				success: false,
				error: "Server error",
			});
		}
	}
}

export {};
