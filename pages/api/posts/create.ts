import type { NextApiRequest, NextApiResponse } from "next";
import {
	fetchAccountByUser,
	getServerSession,
	insertPost,
} from "utils/database";
import { generateRandomSlug } from "utils/functions";

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse<any>
) {
	const session = await getServerSession(req, res);

	if (!session) return res.status(401).json({ message: "Invalid session (not logged in)" });

	const account = await fetchAccountByUser(session.user);

	if (!account) return res.status(401).json({ message: "Invalid account" });
	if (account.role !== "admin") return res.status(401).json({ message: "You must be an administrator to do this" });

	const slug = generateRandomSlug();
	const accountId = account && account.id;
	const newPost = {
		content: "## Hey, welcome to this page\! \n\n Use Markdown syntax here to type your article\.",
		slug: slug,
		author: accountId,
		title: "Correct horse battery staple",
		description: "This is an example post",
		visibility: "private",
		banner: "",
	};

	let post = await insertPost(newPost);
	if (post) {
		res.status(201).json({
			id: post[0].id,
		});
	}
}
