import type { NextApiRequest, NextApiResponse } from "next";
import { fetchPostsAsPublic, fetchPostsAsUser, getServerSession } from "../../../utils/database";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	const session = await getServerSession(req, res);
	if (!session) {
		return res.status(200).json(await fetchPostsAsPublic());
	}
	return res.status(200).json(await fetchPostsAsUser(session.user));
}