import type { NextPage } from "next";
import Navbar from "../components/nav/navbar";
import { fetchAccountByUser, getServerSession } from "utils/database";
import { Account } from "types/types";
import MetaTags from "components/head/MetaTags";
import PrimaryContainer from "components/layout/PrimaryContainer";
import Header from "components/layout/Header";
import Image from "next/image";
import { Tags } from "react-bootstrap-icons";
import Link from "next/link";

const projects = [
	{
		name: "CPlusPatch CMS",
		href: "https://codeberg.org/CPlusPatch/cpluspatch-cms-next",
		description:
			"CPlusPatch CMS is a fully-featured multi-user content editing system suitale for making basic blogs, with administration tools and full CRUD functionality",
		tags: [
			{
				name: "React",
				color: "bg-sky-100",
				textColor: "text-sky-800",
			},
			{
				name: "TailwindCSS",
				color: "bg-blue-100",
				textColor: "text-blue-800",
			},
			{
				name: "NextJS",
				color: "bg-gray-100",
				textColor: "text-gray-800",
			},
			{
				name: "Yarn",
				color: "bg-green-100",
				textColor: "text-green-800",
			},
			{
				name: "Supabase",
				color: "bg-teal-100",
				textColor: "text-teal-800",
			},
		],
	},
	{
		name: "Matrix Homeserver",
		href: "https://element.cplushpatch.party",
		description:
			"The Matrix federated protocol allows for secure end-to-end encrypted modern communication between users. I am hosting my own homeserver for better privacy and speed.",
		tags: [
			{
				name: "Privacy",
				color: "bg-red-100",
				textColor: "text-red-800",
			},
			{
				name: "Decentralized",
				color: "bg-black",
				textColor: "text-white",
			},
			{
				name: "Federation",
				color: "bg-orange-100",
				textColor: "text-orange-800",
			},
		],
	},
	{
		name: "CPlusPatch Web",
		href: "https://codeberg.org/CPlusPatch/cpluspatch-cms-next",
		description:
			"My personal website, part of CPlusPatch CMS. It is a modern application written from the ground up with Next.js and TailwindCSS, incorporated into my CMS.",
		tags: [
			{
				name: "React",
				color: "bg-sky-100",
				textColor: "text-sky-800",
			},
			{
				name: "TailwindCSS",
				color: "bg-blue-100",
				textColor: "text-blue-800",
			},
			{
				name: "NextJS",
				color: "bg-gray-100",
				textColor: "text-gray-800",
			},
			{
				name: "Yarn",
				color: "bg-green-100",
				textColor: "text-green-800",
			},
		],
	},
	{
		name: "GAREF Orbit Camera",
		href: "https://github.com/CPlusPatch/garef-rpi-gopro-interface",
		description:
			"This application is a simple Python application to control an onboard GoPro 10 aboard a satellite to be launched in 2023. It takes images and communicates with a chip to beam them back to Earth.",
		tags: [
			{
				name: "Python",
				color: "bg-yellow-100",
				textColor: "text-yellow-800",
			},
			{
				name: "GoPro",
				color: "bg-slate-100",
				textColor: "text-slate-800",
			},
			{
				name: "Aerospace",
				color: "bg-indigo-100",
				textColor: "text-indigo-800",
			},
		],
	},
	{
		name: "Astro Pi Challenge",
		href: "https://www.esa.int/Education/AstroPI/2016_17_European_Astro_Pi_Challenge_Winners",
		description:
			"Winner of the 2016 Astro Pi international program, written by me at age 10. The program measured atmospheric and magnetic sensor data an logged them to a SenseHAT screen. This program was executed aboard the International Space Station.",
		tags: [
			{
				name: "Python",
				color: "bg-yellow-100",
				textColor: "text-yellow-800",
			},
			{
				name: "Raspberry Pi",
				color: "bg-rose-100",
				textColor: "text-rose-800",
			},
			{
				name: "Aerospace",
				color: "bg-indigo-100",
				textColor: "text-indigo-800",
			},
		],
	},
];

const Home: NextPage = ({ account }: { account: Account | false }) => (
	<div className="relative">
		<MetaTags title={`Projects · ${process.env.NEXT_PUBLIC_AUTHOR_NAME}`} />
		<Navbar account={account} />
		<main>
			<PrimaryContainer className="flex flex-col gap-y-10">
				<Header type="h2">Projects</Header>
				<div>
					<h2 className="text-xs font-medium tracking-wide text-gray-500 uppercase">
						Pinned Projects
					</h2>
					<ul
						role="list"
						className="grid grid-cols-1 gap-5 mt-3 sm:gap-6 sm:grid-cols-2 lg:grid-cols-3">
						{projects.map(project => (
							<li
								key={project.name}
								className="flex col-span-1 rounded-md shadow-sm font-inter">
								<Link
									href={project.href}
									className="flex flex-col justify-between items-start p-3 max-w-full bg-white rounded-md border border-gray-200">
									<h3 className="pb-4 text-lg font-bold text-gray-900 hover:text-gray-600">
										{project.name}
									</h3>
									<p className="text-gray-500">{project.description}</p>
									<div className="flex overflow-scroll flex-row gap-2 mt-3 max-w-full">
										{project.tags.map(tag => (
											<span
												key={tag.name}
												className={`${tag.color} inline-flex items-center px-2 py-0.5 rounded text-xs font-medium ${tag.textColor}`}>
												{tag.name}
											</span>
										))}
									</div>
								</Link>
							</li>
						))}
					</ul>
				</div>
			</PrimaryContainer>
		</main>
	</div>
);

export const getServerSideProps = async ({ req, res }) => {
	const session = await getServerSession(req, res);

	const account: Account | false = session?.user ? await fetchAccountByUser(session.user) : false;

	return {
		props: {
			account: account,
		},
	};
};

export default Home;
