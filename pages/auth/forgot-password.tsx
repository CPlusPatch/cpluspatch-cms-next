import { useSupabaseClient } from "@supabase/auth-helpers-react";
import Button from "components/buttons/Button";
import SmallLogo from "components/logo/SmallLogo";
import Spinner from "components/spinners/Spinner";
import Head from "next/head";
import { useEffect, useState } from "react";

export default function ForgotPassword() {
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const supabase = useSupabaseClient()

	useEffect(() => {
		supabase.auth.onAuthStateChange(async (event, session) => {
			if (event == "PASSWORD_RECOVERY") {
				const newPassword = prompt(
					"What would you like your new password to be?"
				);

				const { data, error } = await supabase.auth.updateUser({
					password: newPassword,
				});

				if (data) alert("Password updated successfully!");

				if (error) alert("There was an error updating your password.");
			}
		});
	});
	
	const onFormSubmit = async (event: any) => {
		event.preventDefault();
		setIsLoading(true);

		const { data, error } = await supabase.auth.resetPasswordForEmail(event.target[0].value, {
			redirectTo: `${process.env.NEXT_PUBLIC_HOSTNAME}/auth/password-recovery`
		})
		
		if (error && process.env.NODE_ENV !== 'production') {
			console.error(error);
		} 

		if (data) {
			alert("Check your email! We just sent you a reset link")
		}
	}
	return (
		<div className="min-h-screen bg-gray-50">
			<Head>
				<title>Welcome! &middot; CPlusPatch 2022</title>
			</Head>

			<div className="flex flex-col justify-center py-12 min-h-screen sm:px-6 lg:px-8">
				<div className="sm:mx-auto sm:w-full sm:max-w-md">
					<div className="flex justify-center w-auto">
						<SmallLogo size="12" />
					</div>
					<h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900 font-poppins">
						Reset your password
					</h2>
				</div>

				<div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
					<div className="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10 font-inter">
						<form className="space-y-6" action="#" method="POST" onSubmit={onFormSubmit}>
							<div>
								<label
									htmlFor="email"
									className="block text-sm font-medium text-gray-700">
									Email address
								</label>
								<div className="mt-1">
									<input
										id="email"
										name="email"
										type="email"
										autoComplete="email"
										required
										placeholder="Your email address here..."
										className="block px-3 py-2 w-full placeholder-gray-400 rounded-md border border-gray-300 shadow-sm duration-200 appearance-none disabled:bg-gray-100 focus:outline-none focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
									/>
								</div>
							</div>

							<div>
								<Button
									type="submit"
									ringColor="orange-500"
									style="orange"
									className="w-full">
									{isLoading ? (
										<Spinner className="w-[1.1rem] h-[1.1rem] text-orange-500 fill-white" />
									) : (
										<>Send reset link</>
									)}
								</Button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}