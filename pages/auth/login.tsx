import Head from "next/head";
import { useSupabaseClient } from "@supabase/auth-helpers-react";
import { fetchAccountByUser, getServerSession } from "utils/database";
import SmallLogo from "components/logo/SmallLogo";
import { useState } from "react";
import Spinner from "components/spinners/Spinner";
import Link from "next/link";
import Button from "components/buttons/Button";
import MetaTags from "components/head/MetaTags";

export default function SignIn() {
	const supabaseClient = useSupabaseClient();
	const [isLoading, setIsLoading] = useState<boolean>(false);

	const loginForm = async (event: any) => {
		event.preventDefault();
		setIsLoading(true);
		console.info("Signing in!")
		let { data, error } = await supabaseClient.auth.signInWithPassword({
			email: event.target[0].value,
			password: event.target[1].value,
		});
		if (error) {
			if (process.env.NODE_ENV !== "production") console.error(error);
			console.error("An error occured while signing in")
			alert("Your password or username is incorrect. Please try again.")
		} else {
			console.info("Signed in successfully")
			window.location.href = "/blog"
		}
		setIsLoading(false);
	}
	
	return (
			<div className="min-h-screen bg-gray-50">
				<MetaTags title="Please sign in" />

				<div className="flex flex-col justify-center py-12 min-h-screen sm:px-6 lg:px-8">
					<div className="sm:mx-auto sm:w-full sm:max-w-md">
						<div className="flex justify-center w-auto">
							<SmallLogo size="12" />
						</div>
						<h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900 font-poppins">
							Sign in to your account
						</h2>
					</div>

					<div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
						<div className="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10 font-inter">
							<form
								className="space-y-6"
								action="#"
								method="POST"
								onSubmit={loginForm}>
								<div>
									<label
										htmlFor="email"
										className="block text-sm font-medium text-gray-700">
										Email address
									</label>
									<div className="mt-1">
										<input
											id="email"
											name="email"
											type="email"
											autoComplete="email"
											required
											placeholder="guest@cpluspatch.com"
											disabled={isLoading}
											className="block px-3 py-2 w-full placeholder-gray-400 rounded-md border border-gray-300 shadow-sm duration-200 appearance-none disabled:bg-gray-100 focus:outline-none focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
										/>
									</div>
								</div>

								<div>
									<label
										htmlFor="password"
										className="block text-sm font-medium text-gray-700">
										Password
									</label>
									<div className="mt-1">
										<input
											id="password"
											name="password"
											type="password"
											autoComplete="current-password"
											required
											disabled={isLoading}
											className="block px-3 py-2 w-full placeholder-gray-400 rounded-md border border-gray-300 shadow-sm duration-200 appearance-none disabled:bg-gray-100 focus:outline-none focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
										/>
									</div>
								</div>

								<div className="flex justify-between items-center">
									<div className="text-sm">
										<Link
											href="/auth/forgot-password"
											className="font-medium text-orange-600 hover:text-orange-500">
											Forgot your password?
										</Link>
									</div>
								</div>

								<div>
									<Button
										type="submit"
										ringColor="orange-500"
										style="orange"
										className="w-full">
										{isLoading ? (
											<Spinner className="w-[1.1rem] h-[1.1rem] text-orange-500 fill-white" />
										) : (
											<>Sign in</>
										)}
									</Button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	);
}

export async function getServerSideProps({ req, res }) {
	const session = await getServerSession(req, res);
	
	if (session) {
		switch ((await fetchAccountByUser(session.user)).role) {
			case "admin":
				return {
					redirect: {
						destination: "/admin/posts",
						permanent: false,
					},
				};
			default:
				return {
					redirect: {
						destination: "/blog",
						permanent: false,
					},
				};
		}
	}
	
	return {
		props: {}
	};
}