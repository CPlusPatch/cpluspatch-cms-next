import { useSupabaseClient } from "@supabase/auth-helpers-react"
import Button from "components/buttons/Button";
import SmallLogo from "components/logo/SmallLogo";
import Spinner from "components/spinners/Spinner";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function PasswordRecovery() {
	const supabase = useSupabaseClient();
	const [showForm, setShowForm] = useState<boolean>(false);
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const router = useRouter();

	useEffect(() => {
		supabase.auth.onAuthStateChange(async (event, session) => {
			if (event == "PASSWORD_RECOVERY") {
				setShowForm(true);
			}
		});
	});

	const onFormSubmit = async (event: any) => {
		const newPassword = event.target[0].value

		const { data, error } = await supabase.auth.updateUser({
			password: newPassword,
		});

		if (data) router.push("/blog")

		if (error) alert("There was an error updating your password.");
	}
	return (
		<div className="min-h-screen bg-gray-50">
			<Head>
				<title>Password recovery! &middot; CPlusPatch 2022</title>
			</Head>

			{showForm && (
				<div className="flex flex-col justify-center py-12 min-h-screen sm:px-6 lg:px-8">
					<div className="sm:mx-auto sm:w-full sm:max-w-md">
						<div className="flex justify-center w-auto">
							<SmallLogo size="12" />
						</div>
						<h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900 font-poppins">
							Reset your password
						</h2>
					</div>

					<div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
						<div className="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10 font-inter">
							<form
								className="space-y-6"
								action="#"
								method="POST"
								onSubmit={onFormSubmit}>
								<div>
									<label
										htmlFor="email"
										className="block text-sm font-medium text-gray-700">
										New password
									</label>
									<div className="mt-1">
										<input
											id="email"
											name="password"
											type="password"
											required
											placeholder="New password"
											className="block px-3 py-2 w-full placeholder-gray-400 rounded-md border border-gray-300 shadow-sm duration-200 appearance-none disabled:bg-gray-100 focus:outline-none focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
										/>
									</div>
								</div>

								<div>
									<Button
										type="submit"
										ringColor="orange-500"
										style="orange"
										className="w-full">
										{isLoading ? (
											<Spinner className="w-[1.1rem] h-[1.1rem] text-orange-500 fill-white" />
										) : (
											<>Reset</>
										)}
									</Button>
								</div>
							</form>
						</div>
					</div>
				</div>
			)}
		</div>
	);
}