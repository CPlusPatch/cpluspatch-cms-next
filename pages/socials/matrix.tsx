import SocialRedirector from "components/socials/Social";
import type { NextPage } from "next";

const Social: NextPage = () => (
	<SocialRedirector link="https://matrix.to/#/@cpluspatch:cplushpatch.party" />
);

export default Social;