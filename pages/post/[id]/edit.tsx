import { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import type { Post, PostOrEmpty, PostWithAccount } from "types/types";
import { isEmpty } from "utils/functions";
import Header from "components/editor/Header";
import { fetchPost, getServerSession } from "utils/database";
import MetaTags from "components/head/MetaTags";
import Editor from "components/editor/Editor";
import Spinner from "components/spinners/Spinner";

const PostEditing = ({ id }) => {
	const [state, setState] = useState<{
		post: PostOrEmpty;
	}>({
		post: {},
	});
	// Global state object containing the post itself, state of UI components and such
	
	useEffect(() => {
		fetch(`/api/posts/${id}`).then(async (response) => {
			const results: {
				success: boolean;
				post: PostOrEmpty;
			} = await response.json();
			setState(s => ({
				...s,
				post: results.post,
			}));
		});
	}, [id]);
	return (
		<div className="relative w-full h-full">
			<Toaster />
			<MetaTags
				title={
					isEmpty(state.post) ? "Loading editor..." : `${state.post.title} · CPlusPatch`
				}
			/>
			<Header state={state} setState={setState} />
			<div className="h-full">
				<div className="flex gap-6 w-full h-full">
					{!isEmpty(state.post) ? (
						<Editor post={state.post as Post} state={state} setState={setState} />
					) : (<div className="flex justify-center items-center w-full h-full">
						<Spinner className="mb-10 w-10 h-10 text-gray-200 fill-orange-500" />
					</div>)}
				</div>
			</div>
		</div>
	);
};

export const getServerSideProps = async ({
	params, req, res }) => {
	const session = await getServerSession(req, res);

	if (!session) {
		return {
			notFound: true,
		};
	}
	const post = await fetchPost("id", params.id);
	if (!post) {
		return {
			notFound: true,
		};
	}

	if ((post as PostWithAccount).author.user_id === session.user.id) {
		return {
			props: {
				id: params.id,
			},
		};
	} else {
		return {
			notFound: true,
		};
	}
	
};

export default PostEditing;
