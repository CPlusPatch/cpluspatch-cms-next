import { getServerSession } from "utils/database";
import Navbar from "components/nav/navbar";
import Footer from "components/footer/footer";
import PrimaryContainer from "components/layout/PrimaryContainer";

interface Props {
	username: string;
}

function Article({ username }: Props) {
	return (
		<div className="w-full min-h-screen bg-gray-50 font-exo">
			{/* <Navbar account={account} /> */}
			<PrimaryContainer size="6xl" className="relative px-0 py-0">
				<div className="flex justify-center mt-8 w-full h-full lg:mt-24 md:mt-16">
					
				</div>
				<Footer />
			</PrimaryContainer>
		</div>
	);
}

export const getServerSideProps = async ({ params, req, res }) => {
	const session = await getServerSession(req, res);

	const { username } = params;

	return {
		props: {
			username: username
		}
	}
};

export default Article;
