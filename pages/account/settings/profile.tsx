/* This example requires Tailwind CSS v2.0+ */
import { useRef, useState } from "react";
import Sidebar from "components/settings/Sidebar";
import type { Account } from "types/types";
import { fetchAccountByUser, getServerSession } from "utils/database";
import Button from "components/buttons/Button";
import { IKImage, IKUpload } from "imagekitio-react";
import toast, { Toaster } from "react-hot-toast";
import Spinner from "components/spinners/Spinner";
import MetaTags from "components/head/MetaTags";


export default function ProfileSettings({ account }: { account: Account }) {
	const [accountState, setAccountState] = useState<Account>(account);
	const [avatar, setAvatar] = useState<string>(account.avatar);
	const [loading, setLoading] = useState<boolean>(false);

	const submit = async (event: any) => {
		setLoading(true);
		event.preventDefault();
		
		fetch(`/api/user/${account.user_id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				name: event.target["name"].value,
				"about_me": event.target["about_me"].value,
			}),
		})
			.then(async data => {
				switch (data.status) {
					case 200: {
						toast.success("Details have been changed!");
						let newState = account;
						newState.name = event.target["name"].value;
						setAccountState(newState);
						break;
					}
					default: {
						toast.error("Couldn't save");
					}
				}
			})
			.catch(error => {
				toast.error("Couldn't save");
			})
			.finally(() => setLoading(false));
	}

	return (
		<div className="flex flex-row w-full h-full bg-gray-50">
			<MetaTags title="Profile Settings · CMS" />
			<Toaster />
			<Sidebar account={accountState} />
			<div className="flex flex-col flex-1 h-full md:pl-64">
				<div className="space-y-6 h-full bg-white sm:px-6 lg:px-0 lg:col-span-9 font-inter">
					<form action="#" method="POST" onSubmit={submit} className="h-full">
						<div className="flex flex-col justify-between h-full shadow md:h-auto md:justify-start sm:overflow-hidden">
							<div className="px-4 py-6 space-y-6 sm:p-6">
								<div>
									<h3 className="text-xl font-medium leading-6 text-gray-900">
										Profile
									</h3>
								</div>

								<div className="grid grid-cols-3 gap-6">
									<div className="col-span-3 sm:col-span-2">
										<label
											htmlFor="company-website"
											className="block text-sm font-medium text-gray-700">
											Name
										</label>
										<div className="flex mt-1 rounded-md">
											<input
												id="name"
												name="name"
												type="text"
												required
												disabled={loading}
												placeholder="User McUsername"
												defaultValue={account.name}
												className="block px-3 py-2 w-full placeholder-gray-400 rounded-md border border-gray-300 shadow-sm duration-200 appearance-none outline-none sm:w-56 disabled:bg-gray-100 focus:outline-none focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
											/>
										</div>
									</div>

									<div className="col-span-3 sm:col-span-2">
										<label
											htmlFor="about_me"
											className="block text-sm font-medium text-gray-700">
											About me
										</label>
										<div className="mt-1">
											<textarea
												rows={4}
												disabled={loading}
												name="about_me"
												id="about_me"
												className="block w-full rounded-md border-gray-300 shadow-sm duration-200 outline-none md:w-80 disabled:bg-gray-100 focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
												defaultValue={account.about_me}
											/>
										</div>
									</div>

									<div className="col-span-3">
										<AvatarEditor
											account={accountState}
											setAccount={setAccountState}
											id={account.user_id}
										/>
									</div>
								</div>
							</div>
							<div className="px-4 py-3 text-left sm:px-6">
								<Button
									type="submit"
									disabled={loading}
									style="orange"
									ringColor="orange-500"
									className="w-full h-10 text-white border-transparent md:w-16">
									{loading ? (
										<Spinner className="w-4 h-4 text-gray-100 fill-orange-400" />
									) : (
										<>Save</>
									)}
								</Button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	);
}

function AvatarEditor({ account, setAccount, id }) {
	const [loading, setLoading] = useState<boolean>(false);
	const inputRef = useRef(null)
	
	return (
		<>
			<label className="block text-sm font-medium text-gray-700">Avatar</label>
			<div className="flex items-center mt-1" id="avatar-upload">
				<div className="relative">
					<IKImage
						className="inline-block overflow-hidden w-16 h-16 bg-gray-100 rounded-md"
						path={account.avatar ?? "blank-avatar.png"}
						transformation={[
							{
								width: "100",
								height: "100",
							},
						]}
					/>
					{loading && (
						<div className="flex absolute inset-0 justify-center items-center rounded-md backdrop-blur-sm bg-gray-500/40">
							<Spinner className="w-6 h-6 text-gray-800 fill-orange-400" />
						</div>
					)}
				</div>
				<div className="inline-block overflow-hidden ml-5">
					{inputRef && (
						<Button
							onClick={() => inputRef.current.click()}
							type="button"
							style="gray"
							className="px-3 leading-4">
							Change
						</Button>
					)}
					<IKUpload
						isPrivateFile={false}
						useUniqueFileName={true}
						onError={console.error}
						disabled={loading}
						inputRef={inputRef}
						onUploadStart={() => {
							setLoading(true);
						}}
						// Only allow images to be uploaded

						multiple={false}
						className="hidden"
						responseFields={["fileName"]}
						onSuccess={response => {
							fetch(`/api/user/${id}`, {
								method: "PUT",
								headers: {
									"Content-Type": "application/json",
								},
								body: JSON.stringify({
									avatar: response.name,
								}),
							})
								.then(async data => {
									switch (data.status) {
										case 200: {
											toast.success("Banner has been changed!");
											let newState = account;
											newState.avatar = response.name;
											setAccount(newState);
											break;
										}
										default: {
											toast.error("Couldn't save");
										}
									}
								})
								.catch(error => {
									toast.error("Couldn't save");
								})
								.finally(() => setLoading(false));
						}}
					/>
				</div>
			</div>
		</>
	);
}

export const getServerSideProps = async ({ req, res }) => {
	const session = await getServerSession(req, res);

	const account: Account | false = session?.user
		? await fetchAccountByUser(session.user)
		: false;

	if (!account) {
		return {
			notFound: true
		}
	}

	return {
		props: {
			account: account,
		},
	};
};