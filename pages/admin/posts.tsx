/* eslint-disable @next/next/no-img-element */
import { useState } from "react";
import { useRouter } from "next/router";
import { PlusIcon } from "@heroicons/react/24/solid";
import PostsFeed from "../../components/blog/Posts";
import { fetchAccountByUser, getServerSession } from "utils/database";
import type { User } from "@supabase/supabase-js";
import type { Account } from "types/types";
import AdminNav from "components/admin-nav/AdminNav";
import MetaTags from "components/head/MetaTags";
import Button from "components/buttons/Button";
import Footer from "components/footer/footer";

function Main({ user, account }: { user: User; account: Account }) {
	return (
		<div className="p-0 m-0 w-full min-h-screen bg-gray-50 duration-200">
			<MetaTags
				title={`Posts · ${process.env.NEXT_PUBLIC_AUTHOR_NAME}`}
				description="Overview of my blog"
				author={process.env.NEXT_PUBLIC_AUTHOR_NAME}
				type="website"
			/>
			<AdminNav current="/admin/posts"/>
			<div className="flex relative flex-col justify-between px-5 mx-auto w-full max-w-6xl min-h-screen lg:pl-24">
				<main className="pt-5">
					<div className="relative mx-auto max-w-7xl">
						<div className="py-5 sm:flex sm:items-center">
							<div className="sm:flex-auto">
								<h1 className="text-4xl font-semibold text-gray-900 font-poppins">
									Posts
								</h1>
								<p className="mt-2 text-sm text-gray-700 font-inter">
									All posts written by everyone will show up
									here
								</p>
							</div>
							<div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
								<NewPostButton />
							</div>
						</div>

						<PostsFeed account={account} />
					</div>
				</main>
				<Footer/>
			</div>
		</div>
	);
}

function NewPostButton() {
	const router = useRouter();
	// Used to prevent people from creating multiple posts by clicking the button several times
	const [isCreatingPost, setIsCreatingPost] = useState<boolean>(false);

	const createNewPost = async () => {
		if (!isCreatingPost) {
			setIsCreatingPost(true);

			const post = await (await window.fetch("/api/posts/create")).json();
			router.push(`/post/${post.id}/edit`);
		}
	};
	return (
		<Button
			onClick={createNewPost}
			type="button"
			style="gray"
			isLoading={isCreatingPost}
			className="px-3 h-10">
			
				<>
					<PlusIcon className="mr-1 w-4 h-4" />
					New post
				</>
		</Button>
	);
}

export const getServerSideProps = async ({ req, res }) => {
	const session = await getServerSession(req, res);

	if (!session) {
		return {
			notFound: true,
		};
	}

	const account = await fetchAccountByUser(session.user);
	if (account.role !== "admin") {
		return {
			notFound: true,
		};
	}

	return {
		props: {
			user: session.user,
			account: account,
		},
	};
};

export default Main;
