import type { Session } from "@supabase/supabase-js";
import AdminNav from "components/admin-nav/AdminNav";
import UsersTable from "components/admin/UsersTable";
import MetaTags from "components/head/MetaTags";
import PrimaryContainer from "components/layout/PrimaryContainer";
import type { Account } from "types/types";
import { fetchAccountByUser, getServerSession } from "utils/database";

interface UsersTypes {
	session: Session;
	account: Account
}

export default function Users({ session, account }: UsersTypes) {
	return (
		<div className="w-full min-h-screen bg-gray-50">
			<MetaTags title="Users" />
			<AdminNav current="/admin/users"/>
			<PrimaryContainer className="w-full h-full">
				<UsersTable/>
			</PrimaryContainer>
		</div>
	);
}

export async function getServerSideProps({req, res}) {
	const session = await getServerSession(req, res);
	
	if (!session) {
		return {
			notFound: true
		}
	}

	const account = await fetchAccountByUser(session.user);
	if (account.role !== "admin") {
		return {
			notFound: true
		}
	}

	return {
		props: {
			session: session,
			account: account,
		}
	}
}