/* eslint-disable @next/next/no-img-element */
import Head from "next/head";
import Navbar from "../../components/nav/navbar";
import PostsFeed from "../../components/blog/Posts";
import { fetchAccountByUser, getServerSession } from "utils/database";
import type { Account } from "types/types";
import MetaTags from "components/head/MetaTags";
import Footer from "components/footer/footer";

function Main({ account }: { account: Account }) {
	return (
		<div className="w-full min-h-screen bg-gray-50 duration-200">
			<Head>
				<MetaTags
					title={`Posts · ${process.env.NEXT_PUBLIC_AUTHOR_NAME}`}
					description="Overview of my blog"
					author={process.env.NEXT_PUBLIC_AUTHOR_NAME}
					type="website"
				/>
			</Head>
			<Navbar account={account} />
			<div className="relative px-5 mx-auto w-full max-w-6xl h-full">
				<main className="mt-10">
					<div className="relative mx-auto max-w-7xl">
						<PostsFeed account={account} />
					</div>
				</main>
				<Footer />
			</div>
		</div>
	);
}

export const getServerSideProps = async ({ req, res }) => {
	const session = await getServerSession(req, res);
	if (session) {
		const account = await fetchAccountByUser(session.user);

		if (account?.role == "admin") {
			return {
				redirect: {
					destination: "/admin/posts",
					permanent: false,
				},
			};
		} else {
			return {
				props: {
					account: account,
				},
			};
		}
	}
	
	return {
		props: {
			account: false,
		},
	};
};

export default Main;
