import Head from "next/head";
import type { Account, PostWithAccount } from "../../types/types";
import { fetchAccountByUser, fetchPost, getServerSession } from "utils/database";
import Navbar from "components/nav/navbar";
import { marked } from "marked";
import { IKImage } from "imagekitio-react";
import Footer from "components/footer/footer";
import PrimaryContainer from "components/layout/PrimaryContainer";
import Header from "components/layout/Header";
import MetaTags from "components/head/MetaTags";
import { CalendarDate, CalendarDateFill, Clock } from "react-bootstrap-icons";

function Article({ post, account, htmlPost }: { post: PostWithAccount, account: Account, htmlPost: string }) {
	return (
		<div className="w-full min-h-screen bg-gray-50 font-exo">
			<MetaTags
				title={post.title}
				description={post.description}
				author={post.author.name}
				image={process.env.NEXT_PUBLIC_IMAGEKIT_URL_ENDPOINT + post.banner}
			/>
			<Navbar account={account} />
			<PrimaryContainer size="6xl" className="relative px-0 py-0">
				<div className="flex flex-col items-center mx-auto mt-8 w-full max-w-4xl h-full lg:mt-24 md:mt-16">
					<article className="px-0 pb-16 w-full h-full">
						<div className="container px-4 mb-12 md:mb-24">
							<Header
								type="h1"
								className="mb-4 text-3xl font-extrabold text-center text-black md:text-5xl">
								{post.title}
							</Header>
						</div>
						{post.banner && (
							<div className="flex overflow-hidden items-center w-full max-h-96 md:rounded-lg">
								<IKImage
									path={post.banner}
									className="relative w-full bg-cover"
									transformation={[
										{
											height: "500",
											width: "800",
										},
									]}
								/>
							</div>
						)}
						<div className="flex gap-x-2 items-center px-4 mx-auto my-5 max-w-2xl font-light text-gray-500 font-inter">
							<CalendarDate className="w-4 h-4" />
							Written on{" "}
							{new Date(post.created_at).toLocaleDateString("en-US", {
								month: "short",
								day: "numeric",
								year: "numeric",
								hour: "numeric",
								minute: "numeric",
							})}
						</div>
						<div
							className="px-4 mx-auto mt-10 max-w-2xl text-gray-700 prose font-inter"
							dangerouslySetInnerHTML={{
								__html: htmlPost,
							}}></div>
					</article>
					{/* <div className="flex flex-col px-4 mb-4 w-full max-w-md">
						<Header type="h3" className="!text-2xl">
							About the author
						</Header>
						<div className="container flex items-center w-full font-inter">
							<IKImage
								path={post.author.avatar}
								className="mr-4 w-16 h-auto rounded-md"
								alt="Avatar of Writer"
								transformation={[
									{
										height: "200",
										width: "200",
									},
								]}
							/>
							<div className="flex flex-col justify-between h-full text-sm">
								<h5 className="pt-1 text-lg leading-none text-gray-900">
									{post.author.name}
								</h5>
								<p className="text-gray-600">@{post.author.username}</p>
							</div>
						</div>
						<div className="container flex mt-3 w-full text-sm font-inter">
							{post.author.about_me}
						</div>
					</div> */}
				</div>
				<Footer />
			</PrimaryContainer>
		</div>
	);
}

export const getServerSideProps = async ({ params, req, res }) => {
	const session = await getServerSession(req, res);
	
	const { slug } = params;
	const post = await fetchPost("slug", slug);

	if (!post) return {
		notFound: true,
	};

	switch (post.visibility) {
		case "unlisted":
		case "public":
			return {
				props: {
					post: post,
					account: session ? await fetchAccountByUser(session.user) : false,
					htmlPost: marked(post.content),
				},
			};
		case "private":
			const account = session ? await fetchAccountByUser(session.user) : false;
			if (!account) {
				return {
                    notFound: true,
                };
			} else {
				if (post.author.user_id === account.user_id) {
					return {
						props: {
							post: post,
							account: account,
							htmlPost: marked(post.content),
						},
					};
				}
			}
	}

	
};

export default Article;