import 'styles/globals.css';
import Head from 'next/head';
import NextNProgress from "nextjs-progressbar";
import { createBrowserSupabaseClient, Session } from '@supabase/auth-helpers-nextjs';
import { useState } from 'react';
import { SessionContextProvider } from '@supabase/auth-helpers-react';
import { AppProps } from 'next/app';
import { IKContext } from 'imagekitio-react';

function App({ Component, pageProps }: AppProps<{
  initialSession: Session
}>) {
	const [supabaseClient] = useState(() => createBrowserSupabaseClient());
	return (
		<>
			<SessionContextProvider
				supabaseClient={supabaseClient}
				initialSession={pageProps.initialSession}>
				<Head>
					<meta
						name="viewport"
						content="width=device-width, initial-scale=1"
					/>
				</Head>
				<NextNProgress />
				<IKContext
					urlEndpoint={process.env.NEXT_PUBLIC_IMAGEKIT_URL_ENDPOINT}
					publicKey={process.env.NEXT_PUBLIC_IMAGEKIT_PUBLIC_KEY}
					authenticationEndpoint={`${process.env.NEXT_PUBLIC_HOSTNAME}/api/auth/upload-auth`}>
					<Component {...pageProps} />
				</IKContext>
			</SessionContextProvider>
		</>
	);
}

export default App
