import type { NextPage } from "next";
import Navbar from "../components/nav/navbar";
import { fetchAccountByUser, getServerSession } from "utils/database";
import { Account } from "types/types";
import MetaTags from "components/head/MetaTags";
import PrimaryContainer from "components/layout/PrimaryContainer";
import Header from "components/layout/Header";
import Image from "next/image";
import onePlus from "public/static/devices/oneplus_mockup.png";
import laptop from "public/static/devices/laptop-mockup.png";
import asus from "public/static/devices/asus.png";

const onePlusList = [
	{
		title: "Operating System",
		value: "LineageOS 20 (Android 13)",
	},
	{
		title: "Play Services Backend",
		value: "microG",
	},
	{
		title: "App Store",
		value: "F-Droid",
	},
	{
		title: "Root Provider",
		value: "Magisk",
	},
];

const gatewayList = [
	{
		title: "Operating System",
		value: "Arch Linux",
	},
	{
		title: "Specs",
		value: "16 GB RAM, 4x i5 @ 2.2 GHz, 512 GB SSD",
	},
	{
		title: "Preferred Package Format",
		value: "Pacman",
	},
	{
		title: "Proprietary Apps",
		value: "Two (Spotify, Steam)",
	},
];

const chunkyList = [
	{
		title: "Operating System",
		value: "Arch Linux",
	},
	{
		title: "RAM",
		value: "32 GB DDR4 RAM",
	},
	{
		title: "CPU",
		value: "8x i7-6700K @ 4.20 GHz",
	},
	{
		title: "GPU",
		value: "GTX 1070 8 GB VRAM",
	},
	{
		title: "Storage",
		value: "240 GB SSD, 512 GB SSD, 2x 2 TB HDD",
	},
];

const Home: NextPage = ({ account }: { account: Account | false }) => {
	
	return (
		<div className="relative">
			<MetaTags title={`Devices · ${process.env.NEXT_PUBLIC_AUTHOR_NAME}`} />
			<Navbar account={account} />
			<main>
				<PrimaryContainer className="flex flex-col gap-y-10">
					<Header type="h2">Devices</Header>
					<div className="items-center lg:grid lg:grid-cols-2 lg:grid-flow-col-dense lg:gap-x-24 lg:px-5">
						<div className="px-4 mx-auto sm:px-6 lg:py-16 lg:mx-0 lg:px-0">
							<div className="flex flex-col gap-y-4">
								<h2 className="text-3xl font-extrabold tracking-tight text-gray-900 font-inter">
									OnePlus 7
								</h2>
								<dl className="max-w-md text-gray-900 divide-y divide-gray-200 font-inter dark:text-white dark:divide-gray-700">
									{onePlusList.map(item => (
										<div className="flex flex-col py-3" key={item.title}>
											<dt className="mb-1 text-gray-500 md:text-lg dark:text-gray-400">
												{item.title}
											</dt>
											<dd className="text-lg font-semibold">{item.value}</dd>
										</div>
									))}
								</dl>
							</div>
						</div>
						<div className="hidden flex-col justify-center items-center mt-0 md:flex">
							<div className="w-auto h-[30rem] rounded-3xl shadow-2xl rotate-12">
								<Image
									className="w-full h-full"
									src={onePlus}
									alt="OnePlus 7"
									width={416}
									height={671}
									priority
									sizes="(max-width: 768px) 100vw,
									(max-width: 1200px) 50vw,
									33vw"
								/>
							</div>
						</div>
					</div>

					<div className="items-center lg:grid lg:grid-cols-2 lg:grid-flow-col-dense lg:gap-x-24 lg:px-5">
						<div className="hidden flex-col justify-center items-center mt-0 md:flex">
							<div className="w-auto rounded-3xl">
								<Image
									className="w-full h-full"
									src={laptop}
									alt="OnePlus 7"
									width={416}
									height={671}
									priority
									sizes="(max-width: 768px) 0vw,
									(max-width: 1200px) 50vw"
								/>
							</div>
						</div>
						<div className="px-4 mx-auto sm:px-6 lg:py-16 lg:mx-0 lg:px-0">
							<div className="flex flex-col gap-y-4">
								<h2 className="text-3xl font-extrabold tracking-tight text-gray-900 font-inter">
									Gateway GWTNT141-10BL
								</h2>
								<dl className="max-w-md text-gray-900 divide-y divide-gray-200 font-inter dark:text-white dark:divide-gray-700">
									{gatewayList.map(item => (
										<div className="flex flex-col py-3" key={item.title}>
											<dt className="mb-1 text-gray-500 md:text-lg dark:text-gray-400">
												{item.title}
											</dt>
											<dd className="text-lg font-semibold">{item.value}</dd>
										</div>
									))}
								</dl>
							</div>
						</div>
					</div>

					<div className="items-center lg:grid lg:grid-cols-2 lg:grid-flow-col-dense lg:gap-x-24 lg:px-5">
						<div className="px-4 mx-auto sm:px-6 lg:py-16 lg:mx-0 lg:px-0">
							<div className="flex flex-col gap-y-4">
								<h2 className="text-3xl font-extrabold tracking-tight text-gray-900 font-inter">
									C H U N K Y
								</h2>
								<dl className="max-w-md text-gray-900 divide-y divide-gray-200 font-inter dark:text-white dark:divide-gray-700">
									{chunkyList.map(item => (
										<div className="flex flex-col py-3" key={item.title}>
											<dt className="mb-1 text-gray-500 md:text-lg dark:text-gray-400">
												{item.title}
											</dt>
											<dd className="text-lg font-semibold">{item.value}</dd>
										</div>
									))}
								</dl>
							</div>
						</div>
						<div className="hidden flex-col justify-center items-center mt-0 md:flex">
							<div className="w-auto h-[35rem] rounded-3xl">
								<Image
									className="w-full h-full"
									src={asus}
									alt="Asus Z170 Pro Gaming motherboard (I have the regular Z170 Pro Model)"
									width={416}
									height={671}
									priority
									sizes="(max-width: 768px) 100vw,
									(max-width: 1200px) 50vw,
									33vw"
								/>
							</div>
						</div>
					</div>
				</PrimaryContainer>
			</main>
		</div>
	);
};

export const getServerSideProps = async ({ req, res }) => {
	const session = await getServerSession(req, res);

	const account: Account | false = session?.user ? await fetchAccountByUser(session.user) : false;

	return {
		props: {
			account: account,
		},
	};
};

export default Home;
