import toast from "react-hot-toast";

export function sendPostUpdate(id: number, data: any): Promise<Response> {
	return new Promise((resolve, reject) => {
		fetch(`/api/posts/${id}`, {
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(data),
		})
			.then(data => {
				switch (data.status) {
					case 201:
					case 200: {
						resolve(data);
						break;
					}
					default: {
						reject();
					}
				}
			})
			.catch(err => {
				console.error(err);
				reject();
			});
	});
}

export function sendPostUpdateWithToasts(id: number, data: any): Promise<void> {
	return new Promise((resolve, reject) => {
		const loadingToast = toast.loading("Saving changes...");
	
		sendPostUpdate(id, data)
			.then(data => {
				toast.success("Saved changes!", {
					id: loadingToast,
				});
				resolve();
				
			})
			.catch(() => {
				toast.error("Couldn't save", {
					id: loadingToast,
				});
				reject();
			});
	})
}
