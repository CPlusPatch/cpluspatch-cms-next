import type { Account, Post, PostsWithAccount, PostWithAccount } from "types/types";
import { createClient, PostgrestError, Session, SupabaseClient, User } from "@supabase/supabase-js";
import { createServerSupabaseClient } from "@supabase/auth-helpers-nextjs";
import type { NextApiRequest, NextApiResponse } from "next";
import { Database } from "types/supabase";
import DOMPurify from "isomorphic-dompurify";

if (!process.env.SUPABASE_API_KEY) {
	throw new Error("Please add your Supabase API KEY to .env.local");
}
if (!process.env.NEXT_PUBLIC_SUPABASE_URL) {
	throw new Error("Please add your Supabase URL to .env.local");
}

export const client: SupabaseClient<Database, "public", any> = createClient(
	process.env.NEXT_PUBLIC_SUPABASE_URL,
	process.env.SUPABASE_API_KEY,
);

/**
 * Gets the session object for the current user on the server (server-only)
 * @param req
 * @param res
 * @returns
 */
export const getServerSession = async (
	req: NextApiRequest,
	res: NextApiResponse,
): Promise<Session> => {
	const supabase = createServerSupabaseClient({ req, res });

	// Check if we have a session
	const {
		data: { session },
	} = await supabase.auth.getSession();
	return session;
};

/**
 * Fetches all posts from the database
 * @returns Array of all the posts
 */
export const fetchPosts = async (): Promise<PostsWithAccount | false> => {
	let { data, error }: { data: unknown; error: PostgrestError } = await client
		.from("posts")
		.select("*, author ( name, username, avatar, user_id )")
		.order("created_at");
	if (error) {
		console.error(error);
		return false;
	}

	return data as PostsWithAccount;
};

/**
 * Inserts a new post into the database with the specified data
 * @param post Post data to insert (without id or created_at)
 * @returns
 */
export const insertPost = async (
	post: Omit<Omit<Post, "id">, "created_at">,
): Promise<Post | false> => {
	// Sanitize post (just in case)
	for (var element in post) {
		post[element] = DOMPurify.sanitize(post[element]);
	}

	let {
		data,
		error,
	}: {
		data: unknown;
		error: PostgrestError;
	} = await client.from("posts").insert(post).select();

	if (error) {
		if (process.env.NODE_ENV !== "production") {
			console.error(error);
		}
		return false;
	}

	return data as Post;
};

/**
 * Finds the account associated with a Supabase User object
 * @param user User  objectto transform into account object
 * @returns Account fetched or false if not found
 */
export const fetchAccountByUser = async (user: User): Promise<Account | null> => {
	let {
		data,
		error,
	}: {
		data: unknown;
		error: PostgrestError;
	} = await client.from("accounts").select().eq("user_id", user.id);

	if (error) {
		console.error(error);
		return null;
	}

	return data[0] as Account;
};

/**
 * Creates a new user and associated account
 * @param email Email of new user (will be sent confirmation email)
 * @param password Password of new user
 * @param username Username of new user
 * @param name Name of new user
 * @returns boolean, true if successful
 */
export const createNewAccount = async (
	email: string,
	password: string,
	username: string,
	name: string,
): Promise<boolean> => {
	const { data, error } = await client.auth.signUp({
		email: email,
		password: password,
	});

	const user_id = data.user ? data.user.id : null;

	if (!error) {
		let {
			data,
			error,
		}: {
			data: unknown;
			error: PostgrestError;
		} = await client
			.from("accounts")
			.insert({
				name: name,
				username: username,
				avatar: null,
				user_id: user_id,
				role: null,
			})
			.select();

		if (!error) {
			return true;
		}

		console.error(error);
		return false;
	}

	console.error(error);
	return false;
};

/**
 * Deletes a post from its ID
 * @param id ID of post
 * @returns True if successfull
 */
export const deletePost = async (id: string): Promise<boolean> => {
	const { data, error }: { data: unknown; error: PostgrestError } = await client
		.from("posts")
		.delete()
		.eq("id", id);

	if (error) {
		console.error(error);
		return false;
	}
	return true;
};

/**
 * Deletes a user from its ID
 * @param id ID of yser
 * @returns True if successfull
 */
export const deleteUser = async (id: string): Promise<boolean> => {
	const { data, error }: { data: unknown; error: PostgrestError } = await client
		.from("accounts")
		.delete()
		.eq("user_id", id);

	if (error) {
		console.error(error);
		return false;
	}

	const { data: data2, error: error2 } = await client.auth.admin.deleteUser(id);

	if (error2) {
		console.error(error);
		return false;
	}

	return true;
};

/**
 * Fetches ALL the users in database. Does not check permissions!
 * @returns Array of users
 */
export const fetchAllUsers = async (): Promise<Account[] | null> => {
	let {
		data,
		error,
	}: {
		data: unknown;
		error: PostgrestError;
	} = await client.from("accounts").select();

	if (error) {
		console.error(error);
		return null;
	}

	return data as Account[];
};

/**
 * Retrieves all posts that all users have access to
 * @param user User to retrieve posts as
 * @returns
 */
export const fetchPostsAsPublic = async (): Promise<PostsWithAccount | false> => {
	let { data, error }: { data: unknown; error: PostgrestError } =
		await client
			.from("posts")
			.select("*, author!inner( name, username, avatar, user_id )")
			.eq("visibility", "public")
			.order("created_at", {
				ascending: false,
			});
	if (error) {
		console.error(error);
		return false;
	}

	return data as PostsWithAccount;
};

/**
 * Retrieves all posts that the user has access to
 * @param user User to retrieve posts as
 * @returns
 */
export const fetchPostsAsUser = async (user: User): Promise<PostsWithAccount | null | []> => {
	let { data: publicPosts, error: error1 }: { data: PostsWithAccount; error: PostgrestError } =
		await client
			.from("posts")
			.select("*, author!inner( name, username, avatar, user_id )")
			.eq("visibility", "public")
			.neq("author.user_id", user.id)
			.order("created_at", {
				ascending: false,
			});
	if (error1) {
		console.error(error1);
		return null;
	}

	let { data: userPosts, error: error2 }: { data: PostsWithAccount; error: PostgrestError } =
		await client
			.from("posts")
			.select("*, author!inner( name, username, avatar, user_id )")
			.eq("author.user_id", user.id)
			.order("created_at", {
				ascending: false,
			});
	if (error2) {
		console.error(error2);
		return null;
	}

	// Hacky way to merge both objects and remove duplicates
	const merged: PostsWithAccount | [] = [
		...new Map([...publicPosts, ...userPosts].map(item => [item.id, item])).values(),
	];

	// YES IT'S HACKY BUT I DONT WANNA LEARN POSTGREST
	merged.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime())

	return merged;
};

/**
 * Get a specific post based on a column's value
 * @param column
 * @param value
 * @returns The found post, or false if there's an error
 */
export const fetchPost = async (column: string, value: any): Promise<PostWithAccount | false> => {
	let { data, error } = await client
		.from("posts")
		.select("*, author!inner( name, username, avatar, user_id, about_me )")
		.eq(column, value)
		.order("created_at");
	if (error) {
		console.error(error);
		return false;
	}
	return data[0] as PostWithAccount;
};

/**
 * Modifies a post selected with a column's value (e.g. "id")
 * @param column
 * @param value
 * @param data
 * @returns
 */
export const editPost = async (column: string, value: any, data: object): Promise<boolean> => {
	// Sanitize data with DOMPurify
	for (var element in data) {
		data[element] = DOMPurify.sanitize(data[element]);
	}

	const { error } = await client.from("posts").update(data).eq(column, value);

	if (error) {
		console.error(error);
		return false;
	}
	return true;
};

/**
 * Modifies an account selected with a column's value (e.g. "id")
 * @param column
 * @param value
 * @param data
 * @returns
 */
export const editAccount = async (column: string, value: any, data: object): Promise<boolean> => {
	// Sanitize data with DOMPurify
	for (var element in data) {
		data[element] = DOMPurify.sanitize(data[element]);
	}

	const { error } = await client.from("accounts").update(data).eq(column, value);

	if (error) {
		console.error(error);
		return false;
	}
	return true;
};
