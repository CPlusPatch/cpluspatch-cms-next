import { Dialog } from "@headlessui/react";
import { FileEarmarkArrowUpFill, PlusSquareDotted, X } from "react-bootstrap-icons";
import { PostOrEmpty } from "types/types";
import Button from "components/buttons/Button";
import Spinner from "components/spinners/Spinner";
import { sendPostUpdateWithToasts } from "utils/client";
import { Field, Form, Formik, FormikHelpers } from "formik";
import SlideOver from "components/modals/SlideOver";
import { isEmpty } from "utils/functions";
import { useRef, useState } from "react";
import { IKImage, IKUpload } from "imagekitio-react";

const privacySettings = [
	{
		name: "Public access",
		value: "public",
		description: "Everyone can see this post.",
	},
	{
		name: "Unlisted",
		value: "unlisted",
		description: "Everyone with the link will see this post.",
	},
	{
		name: "Private",
		value: "private",
		description: "Only you can see this post.",
	},
];

interface Props {
	open: any;
	setOpen: any;
	state: {
		post: PostOrEmpty;
	};
	setState: any;
}

export default function SettingsSlideOver({ open, setOpen, state, setState }: Props) {
	const save = async (values: any, formikHelpers: FormikHelpers<any>) => {
		let newState = state;
		newState.post.description = values.description;
		newState.post.slug = values.slug;
		newState.post.visibility = values.visibility;
		setState(prevState => newState);
		
		sendPostUpdateWithToasts(state.post.id, {
			description: values.description ,
			slug: values.slug,
			visibility: values.visibility,
			banner: state.post.banner,
		}).finally(() => {
			formikHelpers.setSubmitting(false);
			setOpen(false);
		});
	}

	return (
		<SlideOver open={open} setOpen={setOpen}>
			<div className="w-screen max-w-sm pointer-events-auto">
				<Formik
					initialValues={{
						description: state.post.description,
						slug: state.post.slug,
						visibility: state.post.visibility,
					}}
					validate={values => {
						let errors: any = {};

						if (!values.slug) {
							errors.slug = "This field is required";
						} else if (!values.description) {
							errors.description = "This field is required";
						} else if (!/^[a-z0-9-]+$/gm.test(values.slug)) {
							errors.slug =
								"A slug can only use lowercase letters, number and hyphens";
						}

						return errors;
					}}
					onSubmit={save}>
					{({ isSubmitting, errors, touched }) => (
						<Form className="flex overflow-y-scroll relative flex-col pt-6 h-full bg-white shadow-xl">
							<div className="px-4 sm:px-6">
								<div className="flex justify-between items-start">
									<Dialog.Title className="text-lg font-bold text-gray-900">
										Post
									</Dialog.Title>
									<div className="flex items-center ml-3 h-7">
										<button
											type="button"
											className="text-gray-400 bg-white rounded-md duration-200 outline-none hover:text-gray-500 focus:outline-none active:scale-95"
											onClick={() => setOpen(false)}>
											<span className="sr-only">Close panel</span>
											<X className="w-8 h-8" aria-hidden="true" />
										</button>
									</div>
								</div>
							</div>

							<div className="relative flex-1 px-4 mt-6 sm:px-6">
								<div className="flex overflow-y-scroll absolute inset-0 flex-col gap-y-4 px-4 pb-20 sm:px-6">
									<BannerEditor state={state} setState={setState} />
									
									<div className="col-span-3 sm:col-span-2">
										<label
											htmlFor="description"
											className="block text-sm font-medium text-gray-700">
											Post Description
										</label>
										<div className="mt-1">
											<Field
												as="textarea"
												rows={4}
												disabled={isSubmitting}
												name="description"
												id="description"
												className="block w-full rounded-md border-gray-300 shadow-sm duration-200 outline-none md:w-80 disabled:bg-gray-100 focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
											/>
											{errors.description && touched.description && (
												<span className="block mt-1 text-xs font-medium text-red-700">
													{errors.description.toString()}
												</span>
											)}
										</div>
									</div>

									<div className="col-span-3 sm:col-span-2">
										<label
											htmlFor="slug"
											className="block text-sm font-medium text-gray-700">
											Post Slug
										</label>
										<div className="mt-1">
											<Field
												type="text"
												disabled={isSubmitting}
												name="slug"
												id="slug"
												className="block w-full italic rounded-md border-gray-300 shadow-sm duration-200 outline-none md:w-80 disabled:bg-gray-100 valid:focus:ring-orange-500 valid:focus:border-orange-500 sm:text-sm"
											/>
											{errors.slug && touched.slug && (
												<span className="block mt-1 text-xs font-medium text-red-700">
													{errors.slug.toString()}
												</span>
											)}
										</div>
									</div>

									<fieldset className="col-span-3 sm:col-span-2">
										<legend className="text-sm font-medium text-gray-900">
											Privacy
										</legend>

										<div role="group" className="mt-2 space-y-5">
											{privacySettings.map(option => (
												<div
													className="flex relative items-start"
													key={option.value}>
													<div className="flex absolute items-center h-5">
														<Field
															name="visibility"
															type="radio"
															value={option.value}
															disabled={isSubmitting}
															defaultChecked={
																state.post.visibility ===
																option.value
															}
															className="w-4 h-4 text-orange-600 border-gray-300 ring-0 outline-none focus:outline-none focus:ring-0"
														/>
													</div>
													<div className="pl-7 text-sm">
														<label
															htmlFor="privacy-public"
															className="font-medium text-gray-900">
															{option.name}
														</label>
														<p
															id="privacy-public-description"
															className="text-gray-500">
															{option.description}
														</p>
													</div>
												</div>
											))}
										</div>
										{errors.visibility && touched.visibility && (
											<span className="block mt-1 text-xs font-medium text-red-700">
												{errors.visibility.toString()}
											</span>
										)}
									</fieldset>
								</div>
							</div>

							<div className="flex absolute inset-x-0 bottom-0 flex-shrink-0 justify-end px-4 py-3 bg-white border-t">
								<Button
									type="button"
									style="gray"
									disabled={isSubmitting || !isEmpty(errors)}
									className="disabled:opacity-50 focus:outline-none"
									onClick={() => setOpen(false)}>
									Cancel
								</Button>
								<Button
									type="submit"
									ringColor="orange-500"
									style="orange"
									loading={isSubmitting}
									spinnerClasses="w-4 h-4 fill-red-400"
									disabled={isSubmitting || !isEmpty(errors)}
									className="ml-4 disabled:opacity-50">
									Save
								</Button>
							</div>
						</Form>
					)}
				</Formik>
			</div>
		</SlideOver>
	);
}

function BannerEditor({ state, setState }) {
	const [loading, setLoading] = useState<boolean>(false);
	const inputRef = useRef(null);

	return (
		<div className="col-span-3 sm:col-span-2">
			<label className="block text-sm font-medium text-gray-700"></label>
			<div className="flex flex-col items-center mt-1" id="avatar-upload">
				<div className="relative w-full">
					<div className="inline-block overflow-hidden w-full h-36 bg-gray-100 rounded-md">
						<IKImage
							className="bg-cover"
							path={state.post.banner ?? ""}
							transformation={[
								{
									width: "400",
									height: "200",
								},
							]}
						/>
						{state.post.banner == "" && (
							<div className="flex justify-center items-center w-full h-full text-sm text-center text-gray-600">
								Add a new banner<br/>
								Wide aspect ratio recommended
							</div>
						)}
					</div>
					{loading && (
						<div className="flex absolute inset-0 justify-center items-center rounded-md backdrop-blur-sm bg-gray-500/40">
							<Spinner className="w-8 h-auto text-gray-800 fill-orange-400" />
						</div>
					)}
					{inputRef && (
						<Button
							onClick={() => inputRef.current.click()}
							type="button"
							style="gray"
							className="!absolute right-0 bottom-1 !p-2 m-2 bg-opacity-60 hover:bg-opacity-70">
							<FileEarmarkArrowUpFill className="w-5 h-5" />
						</Button>
					)}
				</div>
				<div className="inline-block overflow-hidden ml-5">
					<IKUpload
						isPrivateFile={false}
						useUniqueFileName={true}
						onError={console.error}
						disabled={loading}
						inputRef={inputRef}
						onUploadStart={() => {
							setLoading(true);
						}}
						// Only allow images to be uploaded

						multiple={false}
						className="hidden"
						responseFields={["fileName"]}
						onSuccess={response => {
							setLoading(false);
							let newState = state;
							newState.post.banner = response.name;
							setState(newState);
						}}
					/>
				</div>
			</div>
		</div>
	);
}