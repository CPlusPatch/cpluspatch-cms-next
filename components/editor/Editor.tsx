import { marked } from "marked";
import { useCallback, useEffect, useRef, useState } from "react";
import type { Post, PostOrEmpty } from "types/types";
import { sendPostUpdateWithToasts } from "utils/client";

export default function Editor({
	post,
	state,
	setState
}: {
	post: Post;
	state: {
		post: PostOrEmpty;
	};
	setState: any;
}) {
	const [content, setContent] = useState<string>(post.content);
	const editorRef = useRef(null);

	const saveDocument = useCallback(async () => {
		await sendPostUpdateWithToasts(post.id, {
			title: state.post.title,
			content: content,
		});

		document.title = `${state.post.title} · CPlusPatch`;
	}, [content, post.id, state.post.title]);

	const cancelDefault = useCallback((e: KeyboardEvent) => {
		// Prevent the default browser "save page" dialog from opening when typing ctrl + s
		if (e.key == "s" && e.ctrlKey) {
			e.preventDefault();
		}
	}, []);

	const save = useCallback((e: KeyboardEvent) => {
		// Save document on Ctrl + S
		if (e.key == "s" && e.ctrlKey) {
			e.preventDefault();
			saveDocument();
		}
	}, [saveDocument]);

	useEffect(() => {
		document.addEventListener("keyup", save, false);
		document.addEventListener("keydown", cancelDefault, false);

		return () => {
			// Remove so react hot refresh doesn't make several functions on every refresh
			document.removeEventListener("keyup", save, false);
			document.removeEventListener("keydown", cancelDefault, false);
		}
	}, [cancelDefault, save]);
	return (
		<div className="grid grid-cols-2 gap-x-4 p-5 pt-20 w-full max-h-screen divide-x-2">
			<textarea
				value={content}
				onChange={e => {setContent(e.target.value)}}
				className="overflow-scroll col-span-1 h-full border-none duration-200 outline-none font-inter focus:outline-none focus:ring-0"
			/>
			<article className="overflow-scroll col-span-1 pt-3 pb-72 h-full font-inter">
				<div className="container mt-12 mb-12 md:mb-24">
					<h1 className="mb-4 text-3xl font-extrabold text-center text-black md:text-5xl font-poppins">
						{state.post.title}
					</h1>
				</div>
				<div
					className="px-4 mx-auto mt-10 max-w-2xl text-gray-700 prose"
					dangerouslySetInnerHTML={{
						__html: marked(content),
					}}></div>
			</article>
		</div>
	);
}
