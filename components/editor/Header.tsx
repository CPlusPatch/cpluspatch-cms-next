import Button from "components/buttons/Button";
import SmallLogo from "components/logo/SmallLogo";
import Link from "next/link";
import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";
import { ThreeDots } from "react-bootstrap-icons";
import type { PostOrEmpty } from "types/types";
import SettingsSlideOver from "./SettingsSlideOver";

interface Props {
	state: {
		post: PostOrEmpty;
	};
	setState: Dispatch<
		SetStateAction<{
			post: PostOrEmpty;
		}>
	>;
}

export default function Header({ state, setState }: Props) {
	const [bannerModal, setBannerModal] = useState<boolean>(false);
	const [slideOver, setSlideOver] = useState<boolean>(false);
	return (
		<>
			<nav className="fixed inset-x-0 top-0 z-30 bg-white shadow">
				<div className="px-2 mx-auto sm:px-6 lg:px-8">
					<div className="flex relative justify-between h-16">
						<div className="flex flex-1 justify-start items-center">
							<Link
								href="/blog"
								className="flex flex-shrink-0 items-center pl-3 sm:pl-0 duration-150 ease-in-out hover:scale-[110%] active:scale-95">
								<SmallLogo size="8" />
							</Link>
							<div className="hidden items-center sm:ml-6 sm:flex sm:space-x-8 font-inter">
								{/* Current: "border-indigo-500 text-gray-900", Default: "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700" */}
								<div className="mt-1 rounded-md border-gray-300 focus-within:border-orange-600">
									<input
										type="text"
										id="titleInput"
										onChange={(e: ChangeEvent<HTMLInputElement>) => {
											let newState = state;
											newState.post.title = e.target.value;
											setState(prevState => newState);
										}}
										disabled={state.post.title === undefined}
										defaultValue={state.post.title}
										className="block w-full rounded-md border-gray-300 shadow-sm duration-200 outline-none disabled:bg-gray-100 focus:ring-orange-500 focus:border-orange-500 sm:text-sm"
										placeholder="Title for your post"
									/>
								</div>
							</div>
							<div className="flex gap-x-1 items-center pt-1 ml-3 h-full font-inter">
								<kbd className="px-2 py-1.5 text-xs font-semibold text-gray-800 bg-gray-50 border border-gray-200 rounded-md dark:bg-gray-600 dark:text-gray-100 dark:border-gray-500">
									Ctrl
								</kbd>
								+
								<kbd className="px-2 py-1.5 text-xs font-semibold text-gray-800 bg-gray-50 border border-gray-200 rounded-md dark:bg-gray-600 dark:text-gray-100 dark:border-gray-500">
									S
								</kbd>to save
							</div>
						</div>
						<div className="flex absolute inset-y-0 right-0 gap-x-4 items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
							<span className="inline-flex relative z-0 rounded-md shadow-sm">
								<Button
									onClick={() => {
										setSlideOver(true);
									}}
									type="button"
									style="gray"
									className="md">
									<ThreeDots className="w-5 h-5" />
								</Button>
							</span>
						</div>
					</div>
				</div>
			</nav>
			<SettingsSlideOver
				open={slideOver}
				setOpen={setSlideOver}
				state={state}
				setState={setState}
			/>
		</>
	);
}
