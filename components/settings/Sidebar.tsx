import LongLogo from "components/logo/LongLogo";
import { IKImage } from "imagekitio-react";
import Link from "next/link";
import { PersonBadge } from "react-bootstrap-icons";
import type { Account } from "types/types";
import { classNames } from "utils/functions";

const navigation = [
	{ name: "Profile", href: "/account/settings/profile", icon: PersonBadge, current: true },
];

export default function Sidebar({ account }: { account: Account; }) {
	return (
		<div className="hidden md:flex md:w-64 md:flex-col md:fixed md:inset-y-0 font-inter">
			{/* Sidebar component, swap this element with another sidebar if you like */}
			<div className="flex flex-col flex-1 min-h-0 bg-white border-r border-gray-200">
				<div className="flex overflow-y-auto flex-col flex-1 pt-5 pb-4">
					<Link href="/blog" className="flex flex-shrink-0 items-center px-4">
						<LongLogo size="8" />
					</Link>
					<nav className="flex-1 px-2 mt-5 space-y-1 bg-white">
						{navigation.map((item) => (
							<a
								key={item.name}
								href={item.href}
								className={classNames(
									item.current
										? "bg-gray-100 text-gray-900"
										: "text-gray-600 hover:bg-gray-50 hover:text-gray-900",
									"group flex items-center px-2 py-2 text-sm font-medium rounded-sm hover:pl-5 duration-300"
								)}>
								<item.icon
									className={classNames(
										item.current
											? "text-gray-500"
											: "text-gray-400 group-hover:text-gray-500",
										"mr-3 flex-shrink-0 h-6 w-6"
									)}
									aria-hidden="true"
								/>
								{item.name}
							</a>
						))}
					</nav>
				</div>
				<div className="flex flex-shrink-0 p-4 border-t border-gray-200">
					<a href="#" className="block flex-shrink-0 w-full group">
						<div className="flex items-center">
							<div>
								<IKImage
									path={account.avatar ?? "blank-avatar.png"}
									className="inline-block w-9 h-9 bg-gray-200 rounded-md"
									transformation={[
										{
											width: "200",
											height: "200",
										},
									]}
								/>
							</div>
							<div className="ml-3">
								<p className="text-sm font-medium text-gray-700 group-hover:text-gray-900">
									{account.name}
								</p>
								<p className="text-xs font-medium text-gray-500 group-hover:text-gray-700">
									@{account.username}
								</p>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	);
}
