import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { ExclamationTriangleIcon } from "@heroicons/react/24/outline";
import { toast } from "react-hot-toast";
import Button from "components/buttons/Button";
import ModalOverlay from "./ModalOverlay";

interface Options {
	execute: () => Promise<string | boolean>;
	open: any;
	setOpen: any;
	onSuccess: () => void;
	successText: string;
	failureText: string;
}

export default function ConfirmationModal({
	open,
	setOpen,
	execute,
	onSuccess,
	successText,
	failureText,
}: Options) {
	const [loading, setLoading] = useState<boolean>(false);

	const submit = async () => {
		setLoading(true);
		
		execute().then(() => {
			toast.success(successText);
		}).catch(error => {
			toast.error(failureText);
		}).finally(() => {
			onSuccess();
			setLoading(false);
			setOpen(false);
		});
	}
	return (
		<Transition.Root show={open} as={Fragment}>
			<Dialog
				as="div"
				className="relative z-50"
				onClose={() => {
					if (!loading) setOpen(false);
				}}>
				<ModalOverlay />

				<div className="overflow-y-auto fixed inset-0 z-10">
					<div className="flex justify-center items-end p-4 min-h-full text-center sm:items-center sm:p-0">
						<Transition.Child
							as={Fragment}
							enter="ease-out duration-300"
							enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
							enterTo="opacity-100 translate-y-0 sm:scale-100"
							leave="ease-in duration-200"
							leaveFrom="opacity-100 translate-y-0 sm:scale-100"
							leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
							<Dialog.Panel className="overflow-hidden relative text-left bg-white rounded-lg shadow-xl transition-all transform sm:my-8 sm:w-full sm:max-w-lg">
								<div className="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
									<div className="sm:flex sm:items-start">
										<div className="flex flex-shrink-0 justify-center items-center mx-auto w-12 h-12 bg-red-100 rounded-full sm:mx-0 sm:h-10 sm:w-10">
											<ExclamationTriangleIcon
												className="w-6 h-6 text-red-600"
												aria-hidden="true"
											/>
										</div>
										<div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
											<Dialog.Title
												as="h3"
												className="text-lg font-bold leading-6 text-gray-900 font-inter">
												Delete this data
											</Dialog.Title>
											<div className="mt-2">
												<p className="text-sm text-gray-500 font-inter">
													Are you sure you want to do this? This is a{" "}
													<strong>PERMANENT ACTION</strong> and cannot be
													reversed
												</p>
											</div>
										</div>
									</div>
								</div>
								<div className="px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6 font-inter">
									<Button
										type="button"
										isLoading={loading}
										spinnerClasses="text-gray-200 fill-red-600 h-4"
										className="w-full text-white bg-red-600 border-transparent outline-none hover:bg-red-700 sm:ml-3 sm:w-auto"
										onClick={() => submit()}>
										Yes, I&apos;m sure!
									</Button>
									<Button
										type="button"
										style="gray"
										className="mt-3 w-full sm:mt-0 sm:ml-3 sm:w-auto"
										onClick={() => setOpen(false)}>
										Cancel
									</Button>
								</div>
							</Dialog.Panel>
						</Transition.Child>
					</div>
				</div>
			</Dialog>
		</Transition.Root>
	);
}
