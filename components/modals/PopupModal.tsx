import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import ModalOverlay from "./ModalOverlay";

export default function PopupModal({ children, open, setOpen, loading }) {
	return (
		<Transition.Root show={open} as={Fragment}>
			<Dialog
				as="div"
				className="overflow-y-auto fixed inset-0 z-50"
				onClose={() => {
					if (!loading) setOpen(false);
				}}>
				<div className="flex justify-center items-end px-4 pt-4 pb-5 min-h-screen text-center sm:block sm:p-0">
					<ModalOverlay />

					{/* This element is to trick the browser into centering the modal contents. */}
					<span
						className="hidden sm:inline-block sm:align-middle sm:h-screen"
						aria-hidden="true">
						&#8203;
					</span>
					<Transition.Child
						as={Fragment}
						enter="ease-out duration-300"
						enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
						enterTo="opacity-100 translate-y-0 sm:scale-100"
						leave="ease-in duration-200"
						leaveFrom="opacity-100 translate-y-0 sm:scale-100"
						leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
						{children}
					</Transition.Child>
				</div>
			</Dialog>
		</Transition.Root>
	);
}
