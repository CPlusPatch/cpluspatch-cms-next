import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import ModalOverlay from "./ModalOverlay";

export default function SlideOver({ children, open, setOpen }) {
	return (
		<Transition.Root show={open} as={Fragment}>
			<Dialog as="div" className="overflow-hidden fixed inset-0 z-50" onClose={setOpen}>
				<div className="overflow-hidden absolute inset-0">
					<ModalOverlay />

					<div className="flex fixed inset-y-0 right-0 pl-10 max-w-full pointer-events-none font-inter">
						<Transition.Child
							as={Fragment}
							enter="transform transition ease-in-out duration-300 sm:duration-300"
							enterFrom="translate-x-full"
							enterTo="translate-x-0"
							leave="transform transition ease-in-out duration-300 sm:duration-300"
							leaveFrom="translate-x-0"
							leaveTo="translate-x-full">
							{children}
						</Transition.Child>
					</div>
				</div>
			</Dialog>
		</Transition.Root>
	);
}