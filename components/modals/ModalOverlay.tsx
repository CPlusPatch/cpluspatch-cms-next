import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";

export default function ModalOverlay() {
	return (
		<Transition.Child
			as={Fragment}
			enter="ease-in-out duration-300"
			enterFrom="opacity-0"
			enterTo="opacity-100"
			leave="ease-in-out duration-300"
			leaveFrom="opacity-100"
			leaveTo="opacity-0">
			<Dialog.Overlay className="modal-overlay" />
		</Transition.Child>
	);
}