import { Transition } from "@headlessui/react";
import Button from "components/buttons/Button";
import { IKImage } from "imagekitio-react";
import { Fragment, useCallback, useEffect, useState } from "react";
import { ArrowClockwise, Clipboard, Plus } from "react-bootstrap-icons";
import { toast, Toaster } from "react-hot-toast";
import { Account } from "types/types";
import { isEmpty } from "utils/functions";
import ConfirmationModal from "../modals/ConfirmationModal";
import UserCreationModal from "./UserCreationModal";

export default function UsersTable() {
	const [data, setData] = useState<Account[] | []>([]);
	const [modalOpen, setModalOpen] = useState<boolean>(false);

	const refresh = useCallback(() => {
		if (window) {
			setData([]);
			fetch("/api/admin/users/")
				.then(async response => {
					const json = await response.json();
					if (isEmpty(json)) {
						//return false;
					}
					const data = json as Account[];

					setData(data);
				})
				.catch(err => {
					toast.error("Couldn't fetch users 🫠");
					console.error(err);
				});
		}
	}, [])

	useEffect(() => {
		refresh();
	}, [refresh]);
	return (
		<div className="flex flex-col">
			<Toaster />
			<UserCreationModal open={modalOpen} setOpen={setModalOpen} />
			<div className="px-4 sm:px-6 lg:px-8">
				<div className="sm:flex sm:items-center">
					<div className="sm:flex-auto">
						<h1 className="text-xl font-semibold text-gray-900 font-poppins">Users</h1>
						<p className="mt-2 text-sm text-gray-700 font-inter">
							A list of all the users in your blog, with user UUID and editing.
						</p>
					</div>
					<div className="flex flex-row gap-x-3 mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
						<Button
							type="button"
							style="gray"
							onClick={() => refresh()}
							className="w-full sm:w-auto !py-2 !px-3">
							<ArrowClockwise className="w-4 h-4" />
						</Button>
						<Button
							type="button"
							style="gray"
							onClick={() => setModalOpen(true)}
							className="w-full sm:w-auto">
							<Plus className="mr-1 w-4 h-4" />
							Add user
						</Button>
					</div>
				</div>

				<div className="flex flex-col mt-8">
					<div className="overflow-x-auto -mx-4 -my-2 sm:-mx-6 lg:-mx-8">
						<div className="inline-block py-2 min-w-full align-middle md:px-6 lg:px-8">
							<div className="overflow-hidden ring-1 ring-black ring-opacity-5 shadow md:rounded-lg">
								<table className="min-w-full font-inter">
									<thead className="bg-white">
										<tr>
											<th
												scope="col"
												className="py-3.5 w-1/4 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
												Name
											</th>
											<th
												scope="col"
												className="px-3 w-1/2 py-3.5 text-left text-sm font-semibold text-gray-900 bg-gray-50">
												UUID
											</th>
											<th
												scope="col"
												className="px-3 w-1/8 py-3.5 text-left text-sm font-semibold text-gray-900">
												Role
											</th>
											<th
												scope="col"
												className="relative w-1/8 py-3.5 pl-3 pr-4 sm:pr-6 bg-gray-50">
												<span className="sr-only">Delete</span>
											</th>
										</tr>
									</thead>
									<tbody className="bg-white">
										{isEmpty(data) ? (
											<>
												<SkeletonRow />
												<SkeletonRow />
												<SkeletonRow />
											</>
										) : (
											data.map((user: Account) => (
												<UserRow user={user} key={user.id} />
											))
										)}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

function SkeletonRow() {
	return (
		<tr>
			<td className="py-4 pr-3 pl-4 text-sm whitespace-nowrap sm:pl-6">
				<div className="flex items-center">
					<div className="flex-shrink-0 w-10 h-10">
						<div className="w-10 h-10 bg-gray-200 rounded animate-pulse"></div>
					</div>
					<div className="ml-4">
						<div className="mb-1 w-32 h-4 font-medium text-gray-900 bg-gray-200 rounded animate-pulse"></div>
						<div className="w-20 h-4 text-gray-500 bg-gray-200 rounded animate-pulse"></div>
					</div>
				</div>
			</td>
			<td className="px-3 py-4 text-sm text-gray-700 bg-gray-50">
				<div className="w-72 h-4 text-gray-500 bg-gray-200 rounded animate-pulse"></div>
			</td>
			<td className="flex justify-center items-center px-3 py-4 text-sm text-gray-500 whitespace-nowrap">
				<div className="w-10 h-4 text-gray-500 bg-gray-200 rounded animate-pulse"></div>
			</td>
			<td className="py-4 pr-4 pl-3 text-sm font-medium text-right whitespace-nowrap bg-gray-50 sm:pr-6">
				<div className="w-10 h-4 text-gray-500 bg-gray-200 rounded animate-pulse"></div>
			</td>
		</tr>
	);
}

function UserRow({ user }: { user: Account }) {
	const [modalOpen, setModalOpen] = useState<boolean>(false);
	const [deleted, setDeleted] = useState<boolean>(false);

	const deleteUser = async () =>
		new Promise<boolean | string>((resolve, reject) => {
			fetch(`/api/admin/users/${user.user_id}/delete`)
				.then(response => {
					if (response.status === 200) {
						resolve(true);
					} else {
						reject(false);
					}
				})
				.catch(error => {
					reject(error);
				});
		});

	return (
		!deleted && (
			<tr>
				<td className="py-4 pr-3 pl-4 text-sm whitespace-nowrap sm:pl-6">
					<ConfirmationModal
						execute={deleteUser}
						open={modalOpen}
						setOpen={setModalOpen}
						onSuccess={() => setDeleted(true)}
						successText="User deleted successfully"
						failureText="There was a problem deleting the user"
					/>
					<div className="flex items-center">
						<div className="flex-shrink-0 w-10 h-10">
							<IKImage
								className="w-10 h-10 bg-gray-200 rounded"
								path={user.avatar ?? "blank-avatar.png"}
								alt="Avatar"
								transformation={[
									{
										width: "200",
										height: "200",
									},
								]}
							/>
						</div>
						<div className="ml-4">
							<div className="font-medium text-gray-900">{user.name}</div>
							<div className="text-gray-500">@{user.username}</div>
						</div>
					</div>
				</td>
				<td className="px-3 py-4 font-mono text-sm text-gray-700 bg-gray-50">
					<UUIDRow id={user.user_id} />
				</td>
				<td className="flex justify-center items-center px-3 py-4 text-sm text-gray-500 whitespace-nowrap">
					{user.role && (
						<span className="bg-green-100 text-green-800 inline-flex items-center px-2 py-0.5 rounded text-xs font-medium">
							{user.role.toUpperCase()}
						</span>
					)}
				</td>
				<td className="py-4 pr-4 pl-3 text-sm font-medium text-right whitespace-nowrap bg-gray-50 sm:pr-6">
					<button
						onClick={() => setModalOpen(true)}
						className="text-red-600 duration-200 hover:text-red-900">
						Delete
						<span className="sr-only">, {user.name}</span>
					</button>
				</td>
			</tr>
		)
	);
}
function UUIDRow({ id }) {
	const [showTooltip, setShowTooltip] = useState<boolean>(false);
	const [tooltipText, setTooltipText] = useState<string>("Copy to clipboard");
	
	return (
		<div className="flex flex-row gap-x-2 items-center hover:underline">
			{id}
			<div className="flex flex-row">
				<Clipboard
					className="w-4 h-auto"
					onMouseEnter={() => {
						setShowTooltip(true);
					}}
					onMouseLeave={() => {
						setShowTooltip(false);
					}}
					onClick={() => {
						navigator.clipboard.writeText(id);
						setTooltipText("Copied!")
						setTimeout(() => {
							setTooltipText("Copy to clipboard");
						}, 3000)
					}}
				/>
				<Transition
					as={Fragment}
					show={showTooltip}
					enter="transform transition duration-150"
					enterFrom="opacity-0 scale-50"
					enterTo="opacity-100 scale-100"
					leave="transform duration-150 transition ease-in-out"
					leaveFrom="opacity-100 scale-100"
					leaveTo="opacity-0 scale-95">
					<div
						role="tooltip"
						className="inline-block absolute z-10 px-2 py-1 ml-5 text-xs font-medium text-gray-900 bg-white rounded-lg border border-gray-200 shadow-sm -translate-y-1.5 font-inter">
						{tooltipText}
					</div>
				</Transition>
			</div>
		</div>
	);
}
