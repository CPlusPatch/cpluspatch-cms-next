import { useState } from "react";
import { Dialog } from "@headlessui/react";
import { UsersIcon } from "@heroicons/react/24/solid";
import { useSupabaseClient } from "@supabase/auth-helpers-react";
import { toast } from "react-hot-toast";
import Button from "components/buttons/Button";
import PopupModal from "components/modals/PopupModal";
import { Input, Label } from "components/forms/Input";

export default function UserCreationModal({ open, setOpen }) {
	const [loading, setLoading] = useState<boolean>(false);
	const supabase = useSupabaseClient();

	const createNewUser = async event => {
		event.preventDefault();
		setLoading(true);

		fetch("/api/admin/users/create", {
			method: "POST",
			body: JSON.stringify({
				name: event.target[0].value,
				username: event.target[1].value,
				email: event.target[2].value,
				password: event.target[3].value,
			}),
		})
			.then(data => {
				switch (data.status) {
					case 201: {
						toast.success("Created user");
						setLoading(false);
						setOpen(false);
						break;
					}

					default: {
						console.error(data.body);
						toast.error("Failed to create user");
						setLoading(false);
						setOpen(false);
						break;
					}
				}
			})
			.catch(error => {
				console.error(error);
				toast.error("Failed to create user");
				setLoading(false);
				setOpen(false);
			});
	};

	return (
		<PopupModal open={open} setOpen={setOpen} loading={loading}>
			<form
				onSubmit={createNewUser}
				action="#"
				method="POST"
				className="inline-block overflow-hidden relative px-4 pt-5 pb-4 w-full text-left align-bottom bg-white rounded-lg shadow-xl transition-all transform sm:my-8 sm:align-middle sm:max-w-sm sm:p-6">
				<div>
					<div className="flex justify-center items-center mx-auto w-12 h-12 bg-orange-100 rounded-full">
						<UsersIcon className="w-6 h-6 text-orange-600" aria-hidden="true" />
					</div>
					<div className="mt-3 sm:mt-5 font-inter">
						<Dialog.Title
							as="h3"
							className="mb-4 text-lg font-bold leading-6 text-center text-gray-900">
							Create a new user
						</Dialog.Title>
						<div className="flex flex-col gap-y-4 mt-2">
							<Input
								isLoading={loading}
								name="name"
								id="name"
								required
								placeholder="User McUsername">
								<Label>Name</Label>
							</Input>

							<Input
								isLoading={loading}
								name="username"
								id="username"
								required
								placeholder="@myusername">
								<Label>Username</Label>
							</Input>

							<Input
								isLoading={loading}
								name="email"
								id="email"
								required
								placeholder="Will be sent a confirmation email">
								<Label>Email address</Label>
							</Input>

							<Input
								isLoading={loading}
								name="password"
								id="password"
								minLength={6}
								type="password"
								required>
								<Label>Password</Label>
							</Input>
						</div>
					</div>
				</div>
				<div className="mt-5 sm:mt-6">
					<Button
						type="submit"
						style="orange"
						isLoading={loading}
						className="w-full border-transparent button">
						Create
					</Button>
				</div>
			</form>
		</PopupModal>
	);
}
