export default function FediVerification() {
	return (
		<>
			<a rel="me" className="sr-only" href="https://social.linux.pizza/@jesse">
				link for fediverse verification
			</a>
			<a rel="me" className="sr-only" href="https://social.cpluspatch.com/@admin">
				link for fediverse verification
			</a>
			<a rel="me" className="sr-only" href="https://social.cpluspatch.com/@jesse">
				link for fediverse verification
			</a>
		</>
	);
}
