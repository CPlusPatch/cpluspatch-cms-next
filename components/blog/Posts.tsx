import { useEffect, useState } from "react";
import type {
	Account,
	PostsWithAccount,
	PostWithAccount,
} from "../../types/types";
import { isEmpty } from "../../utils/functions";
import Link from "next/link";
import { IKImage } from "imagekitio-react";
import { Toaster } from "react-hot-toast";
import ConfirmationModal from "components/modals/ConfirmationModal";
import Button from "components/buttons/Button";
import { FileRichtextFill, TrashFill } from "react-bootstrap-icons";
import imageKitLoader from "utils/image-kit";

export default function PostsFeed({ account }: { account: Account }) {
	const [posts, setPosts] = useState<PostsWithAccount>({});

	useEffect(() => {
		fetch("/api/posts").then(async (response) => {
			const results: PostsWithAccount = await response.json();
			console.log(results);
			setPosts(results);
		});
	}, []);
	return (
		<ul className="flex flex-col gap-y-5 pb-10">
			<Toaster />
			{!isEmpty(posts) ? (
				posts.map((post: PostWithAccount) => (
					<Post
						post={post}
						key={post.id.toString()}
						canEdit={
							account.user_id === post.author.user_id
						}
					/>
				))
			) : (
				<li className="h-48">
					<article className="w-full lg:max-w-full lg:flex">
						<div
							className="overflow-hidden flex-none h-48 text-center bg-center bg-cover rounded-t shimmer lg:h-auto lg:w-64 lg:rounded-t-none lg:rounded-l"></div>
						<div className="flex flex-col justify-between p-4 leading-normal bg-white grow">
							<div className="mb-8">
								<p className="flex items-center mb-1 w-32 h-5 text-sm shimmer"></p>
								<div className="mb-2 w-24 h-5 text-xl font-bold text-gray-900 shimmer"></div>
								<p className="mb-1 w-full h-5 text-base text-gray-700 shimmer"></p>
								<p className="w-full h-5 text-base text-gray-700 shimmer"></p>
							</div>
							<div className="flex items-center">
								<div className="mr-4 w-10 h-10 rounded-md shimmer"></div>
								<div className="text-sm">
									<p className="mb-1 w-40 h-5 leading-none text-gray-900 shimmer"></p>
									<p className="w-12 h-5 text-gray-600 shimmer"></p>
								</div>
							</div>
						</div>
					</article>
				</li>
			)}
		</ul>
	);
}

function Post({ post, canEdit }: { post: PostWithAccount; canEdit: boolean }) {
	const [modalOpen, setModalOpen] = useState<boolean>(false); // Refers to post deletion modal
	const [deleted, setDeleted] = useState<boolean>(false); // Set to true to hide the post (when it is deleted_)

	const deletePost = async () =>
		new Promise<boolean | string>((resolve, reject) => {
			fetch(`/api/posts/${post.id}/delete`)
				.then((response) => {
					if (response.status === 200) {
						resolve(true);
					} else {
						reject(false);
					}
				})
				.catch((error) => {
					reject(error);
				});
		});

	return (
		!deleted && (
			<li>
				<ConfirmationModal
					execute={deletePost}
					open={modalOpen}
					setOpen={setModalOpen}
					onSuccess={() => setDeleted(true)}
					successText="Post deleted successfully!"
					failureText="There was an error deleting this post"
				/>
				<article className="w-full rounded shadow-sm lg:max-w-full lg:flex min-h-[12rem]">
					<div
						className="flex overflow-hidden flex-none items-center h-48 text-center bg-gray-200 bg-center bg-cover rounded-t lg:w-64 lg:rounded-t-none lg:rounded-l lg:h-auto"
						style={{
							backgroundImage: `url(${imageKitLoader({
								src: post.banner,
								width: 400,
								quality: 100,
							})})`,
						}}></div>
					<div className="flex flex-col justify-between p-4 leading-normal bg-white grow">
						<Link className="mb-4" href={`/blog/${post.slug}`}>
							<p className="flex items-center text-sm font-bold text-gray-600 font-inter">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 20 20"
									fill="currentColor"
									className="mr-2 w-5 h-5 fill-current">
									<path
										fillRule="evenodd"
										d="M4.5 2A1.5 1.5 0 003 3.5v13A1.5 1.5 0 004.5 18h11a1.5 1.5 0 001.5-1.5V7.621a1.5 1.5 0 00-.44-1.06l-4.12-4.122A1.5 1.5 0 0011.378 2H4.5zm2.25 8.5a.75.75 0 000 1.5h6.5a.75.75 0 000-1.5h-6.5zm0 3a.75.75 0 000 1.5h6.5a.75.75 0 000-1.5h-6.5z"
										clipRule="evenodd"
									/>
								</svg>
								ARTICLE
							</p>
							<div className="mb-2 text-xl font-semibold text-gray-900 font-inter">
								{post.title}
							</div>
							<p className="text-base text-gray-700 font-inter">{post.description}</p>
						</Link>
						<div className="flex items-center font-inter">
							<IKImage
								path={post.author.avatar}
								className="mr-4 w-10 h-10 rounded-md"
								alt="Avatar of Writer"
								transformation={[
									{
										height: "80",
										width: "80",
									},
								]}
							/>
							<div className="flex flex-col justify-between h-full text-sm">
								<p className="pt-1 leading-none text-gray-900">
									{post.author.name}
								</p>
								<p className="text-gray-600">{(new Date(post.created_at)).toLocaleDateString("en-US", {
									month: "short",
									day: "2-digit",
									year: "2-digit"
								})}</p>
							</div>
							{canEdit && (
								<div className="flex flex-row gap-3 mr-4 ml-auto">
									<Link href={`/post/${post.id.toString()}/edit`}>
										<Button style="gray" className="px-2 py-2">
											<FileRichtextFill className="w-6 h-6" />
										</Button>
									</Link>

									<Button
										onClick={() => setModalOpen(true)}
										style="gray"
										className="px-2 py-2 text-rose-500">
										<TrashFill className="w-6 h-6" />
									</Button>
								</div>
							)}
						</div>
					</div>
				</article>
			</li>
		)
	);
}
