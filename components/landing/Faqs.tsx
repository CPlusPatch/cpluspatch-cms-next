import { Disclosure, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/24/outline';
import nl2br from "react-nl2br";
import { classNames } from 'utils/functions';
import branding from "../../config/branding.json";
import PrimaryContainer from '../layout/PrimaryContainer';

export default function Faqs() {
	return (
		<PrimaryContainer>
			<div className="mx-auto max-w-3xl divide-y-2 divide-gray-200">
				<h2 className="text-3xl font-extrabold text-center text-gray-900 dark:text-gray-200 sm:text-4xl font-poppins">
					Frequently asked questions
				</h2>
				<dl className="mt-6 space-y-6 divide-y divide-gray-200">
					{branding.faq.map((faq) => (
						<Disclosure key={faq.question}>
							{({ open }) => (
								<div className="pt-6">
									<dt className="text-lg">
										<Disclosure.Button className="flex justify-between items-start w-full text-left text-gray-400 font-inter">
											<span className="font-medium text-gray-900 dark:text-gray-300">
												{nl2br(faq.question)}
											</span>
											<span className="flex items-center ml-6 h-7">
												<ChevronDownIcon
													className={classNames(
														open
															? "-rotate-180"
															: "rotate-0",
														"w-6 h-6 duration-300 ease-in-out transform"
													)}
													aria-hidden="true"
												/>
											</span>
										</Disclosure.Button>
									</dt>
									<Transition
										enter="transition duration-100 ease-out"
										enterFrom="transform scale-95 opacity-0"
										enterTo="transform scale-100 opacity-100"
										leave="transition duration-75 ease-out"
										leaveFrom="transform scale-100 opacity-100"
										leaveTo="transform scale-95 opacity-0">
										<Disclosure.Panel>
											<dd className="pr-12 mt-2">
											<p className="text-base text-gray-500 dark:text-gray-400">
												{nl2br(faq.answer)}
											</p>
											</dd>
										</Disclosure.Panel>
									</Transition>
								</div>
							)}
						</Disclosure>
					))}
				</dl>
			</div>
		</PrimaryContainer>
	);
}
